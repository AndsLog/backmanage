const express = require('express');
const router = express.Router();

const certificateCtl = require('../controllers/certificate');
const AI_trainingCtl = require('../controllers/AI_training');
const fileUploadCtl = require('../controllers/fileUpload');
const usageCtl = require('../controllers/usage');

router.use(function(req, res, next) {
    var oneof = false;
    if (req.headers.origin) {
        res.header('Access-Control-Allow-Origin', req.headers.origin);
        oneof = true;
    }
    if (req.headers['access-control-request-method']) {
        res.header('Access-Control-Allow-Methods', req.headers['access-control-request-method']);
        oneof = true;
    }
    if (req.headers['access-control-request-headers']) {
        res.header('Access-Control-Allow-Headers', req.headers['access-control-request-headers']);
        oneof = true;
    }
    if (oneof) {
        res.header('Access-Control-Max-Age', 60 * 60 * 24 * 365);
    }

    // intercept OPTIONS method
    if (oneof && req.method == 'OPTIONS') {
        res.send(200);
    } else {
        next();
    }
});

router.post('/backstage/oauth', certificateCtl.oauth);
router.post('/backstage/organizations', certificateCtl.getOrganizations);
router.post('/backstage/spaces/:orgguid', certificateCtl.getSpaces);
router.post('/backstage/services/:spaceguid', certificateCtl.getServices);
router.post('/backstage/watsonassistant/services/:serviceguid', certificateCtl.getWatsonList);
router.post('/backstage/watsonassistant/:name/workspace', certificateCtl.getWorkspace);

router.post('/backstage/watsonassistant/:name/say/:serviceguid', AI_trainingCtl.getWatsonSay);
router.post('/backstage/watsonassistant/:name/log/:serviceguid', AI_trainingCtl.getWatsonTrainingLog);
router.post('/backstage/watsonassistant/:name/workspace/:workspaceid/services/:serviceguid/push', AI_trainingCtl.pushExample);

router.post('/backstage/usage', usageCtl.fetchDailyUsage);
router.post('/backstage/usage/daily/org/:orgguid/space/:spaceguid', usageCtl.getDailyUsage);


router.post('/backstage/watsonassistant/:waserviceguid/workspace/:workspaceid/services/:dbserviceguid/uploadfile', fileUploadCtl.uploadFileToTrain);

module.exports = router;