var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/login', function(req, res, next) {
  res.render('login', { title: '登入' });
});

router.get('/home', function(req, res, next) {
  res.render('home', { title: '首頁' });
});

module.exports = router;
