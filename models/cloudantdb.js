(function () {
    const Cloudant = require('@cloudant/cloudant');
    // var cloudantConfig = {};
    function cloudantDBModel() {};

    cloudantDBModel.prototype.initDB = function(username, password) {
        return new Promise((resolve, reject) => {
            let cloudantConfig = {
                account: username,
                password: password,
                plugins: 'promises'
            }
            // cloudantConfig.account = username;
            // cloudantConfig.password = password;
            // cloudantConfig.plugins = 'promises';
            let cloudant = Cloudant(cloudantConfig);
            return resolve(cloudant);
        }).catch(() => {
            return null;
        });
    }

    cloudantDBModel.prototype.createDB = function(cloudant, name) {
        // let cloudant = Cloudant(cloudantConfig);
        return cloudant.db.create(name).then((data) => {
            return data;
        }).catch((error) => {
            console.log(error);
            return null;
        });
    }

    cloudantDBModel.prototype.find = function(cloudant, tableName, query) {
        // let cloudant = Cloudant(cloudantConfig);
        let table = cloudant.use(tableName);
        return table.find(query).then((data) => {
            if(data.docs instanceof Array && 0 === data.docs.length) {
                return {};
            }
            return data;
        }).catch((error) => {
            console.log(error);
            return null;
        });
    }

    cloudantDBModel.prototype.get = function(cloudant, tableName) {
        // let cloudant = Cloudant(cloudantConfig);
        let table = cloudant.use(tableName);
        return table.get().then((data) => {
            return data;
        }).catch((error) => {
            console.log(error);
            return null;
        });
    }

    cloudantDBModel.prototype.view = function(cloudant, tableName, viewName) {
        // let cloudant = new Cloudant(cloudantConfig);
        let table = cloudant.use(tableName);
        return table.view(viewName, viewName, { 
            reduce:true,
            group:true
        }).then((data) => {
            return data;
        }).catch((error) => {
            console.log(error);
            return null;
        });
    }

    cloudantDBModel.prototype.DBindex = function(cloudant, tableName, indexContent) {
        // let cloudant = new Cloudant(cloudantConfig);
        let table = cloudant.use(tableName);
        return table.index(indexContent).then((data) => {
            return data;
        }).catch((error) => {
            console.log(error);
            return null;
        });
    }

    cloudantDBModel.prototype.updateIntent = function(cloudant, tableName, putData) {
        // let cloudant = new Cloudant(cloudantConfig);
        let table = cloudant.use(tableName);
        return table.insert(putData).then((result) => {
            return result;
        }).catch((error) => {
            console.log(error);
            return null;
        });
    }

    cloudantDBModel.prototype.insert = function(cloudant, tableName,pushData) {
        // let cloudant = new Cloudant(cloudantConfig);
        let table = cloudant.use(tableName);
        return table.insert(pushData).then((result) => {
            return result;
        }).catch((error) => {
            console.log(error);
            return null;
        });
    }

    module.exports = new cloudantDBModel();
}());