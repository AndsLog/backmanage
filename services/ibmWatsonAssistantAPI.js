(function(){
    const AssistantV1 = require('watson-developer-cloud/assistant/v1');
    // var watsonConfig = {};

    function ibmWAAPI() {};

    ibmWAAPI.prototype.initWatson = function(username, password) {
        return new Promise((resolve, reject) => {
            let watsonConfig = {
                username: username,
                password: password,
                version: '2018-02-16'
            }
            // watsonConfig.username = username;
            // watsonConfig.password = password;
            // watsonConfig.version = '2018-02-16';
            let assistant = new AssistantV1(watsonConfig);
            return resolve(assistant);
        }).catch(() => {
            return null;
        });
    }

    ibmWAAPI.prototype.listIntent = function(assistant, workspaceId) {
        let listIntent = {
            workspace_id: workspaceId
        };
        // let assistant = new AssistantV1(watsonConfig);
        return new Promise((resolve, reject) => {
            assistant.listIntents(listIntent, (error, intents) => {
                if(error) {
                    return reject(new Error(error));
                }
                return resolve(intents);
            });
        }).catch((error) => {
            console.log(error);
            return null;
        });
    }

    ibmWAAPI.prototype.listWorkspace = function(assistant) {
        // let assistant = new AssistantV1(watsonConfig);
        return new Promise((resolve, reject) => {
            assistant.listWorkspaces((error, workspaces) => {
                if(error) {
                    return reject(new Error(error));
                }
                return resolve(workspaces);
            });
        }).catch((error) => {
            console.log(error);
            return null;
        })
    }

    ibmWAAPI.prototype.getExample = function(assistant, workspaceId, intent, text) {
        // let assistant = new AssistantV1(watsonConfig);
        return new Promise((resolve, reject) => {
            if (intent === 'irrelevant') {
                let getConterExample = {
                    workspace_id: workspaceId,
                    text: text
                }
                assistant.getCounterexample(getConterExample, (error, result) => {
                    if(error) {
                        return reject(new Error(error));
                    }
                    return resolve(result);
                });
            } else {
                let getExample = {
                    workspace_id: workspaceId,
                    intent: intent,
                    text: text
                }
                assistant.getExample(getExample, (error, result) => {
                    if(error) {
                        return reject(new Error(error));
                    }
                    return resolve(result);
                });
            }
        }).catch((error) => {
            console.log(error);
            return null;
        })
    }

    ibmWAAPI.prototype.deleteExample = function(assistant, workspaceId, intent, text){
        // let assistant = new AssistantV1(watsonConfig);
        return new Promise((resolve, reject) => {
            if (intent === 'irrelevant') {
                let deleteConterExample = {
                    workspace_id: workspaceId,
                    text: text
                }
                assistant.deleteCounterexample(deleteConterExample, (error, result) => {
                    if(error) {
                        return reject(new Error(error));
                    }
                    return resolve(result);
                });
            } else {
                let deleteExample = {
                    workspace_id: workspaceId,
                    intent: intent,
                    text: text
                }
                assistant.deleteExample(deleteExample, (error, result) => {
                    if(error) {
                        return reject(new Error(error));
                    }
                    return resolve(result);
                });
            }
            
        }).catch((error) => {
            console.log(error);
            return null;
        })
    }

    ibmWAAPI.prototype.createExample = function(assistant, workspaceId, intent, text) {
        // let assistant = new AssistantV1(watsonConfig);
        return new Promise((resolve, reject) => {
            if (intent === 'irrelevant') {
                let pushConterExample = {
                    workspace_id: workspaceId,
                    text: text
                };
                assistant.createCounterexample(pushConterExample, (error, result) => {
                    if(error) {
                        return reject(new Error(error));
                    }
                    return resolve(result);
                });
            } else {
                let pushExample = {
                    workspace_id: workspaceId,
                    intent: intent,
                    text: text
                };
                assistant.createExample(pushExample, (error, result) => {
                    if(error) {
                        return reject(new Error(error));
                    }
                    return resolve(result);
                });
            }
            
        }).catch((error) => {
            console.log(error);
            return null;
        })
    }

    ibmWAAPI.prototype.getIntent = function(assistant, workspaceId, intent) {
        let getIntent = {
            intent: intent,
            workspace_id: workspaceId
        };
        // let assistant = new AssistantV1(watsonConfig);
        return new Promise((resolve, reject) => {
            assistant.getIntent(getIntent, (error, data) => {
                if(error) {
                    return reject(new Error(error));
                }
                return resolve(data);
            });
        }).catch((error) => {
            console.log(error);
            return null;
        })
    }

    ibmWAAPI.prototype.insertIntent = function(assistant, workspaceId, pushIntent) {
        pushIntent.workspace_id = workspaceId;
        // let assistant = new AssistantV1(watsonConfig);
        return new Promise((resolve, reject) => {
            assistant.createIntent(pushIntent, (error, data) => {
                if(error) {
                    return reject(new Error(error));
                }
                return resolve(pushIntent);
            });
        }).catch((error) => {
            console.log(error);
            let errorLog = {
                type: 'intent',
                action: 'insert',
                message: error.message,
                value: pushIntent.intent,
                timestamp: Date.now()
            }
            return errorLog;
        })
    }

    ibmWAAPI.prototype.putIntent = function(assistant, workspaceId, putIntent) {
        putIntent.workspace_id = workspaceId;
        // let assistant = new AssistantV1(watsonConfig);
        return new Promise((resolve, reject) => {
            assistant.updateIntent(putIntent, (error, data) => {
                if(error) {
                    return reject(new Error(error));
                }
                return resolve(putIntent);
            });
        }).catch((error) => {
            console.log(error);
            let errorLog = {
                type: 'intent',
                action: 'update',
                message: error.message,
                value: putIntent.intent,
                timestamp: Date.now()
            }
            return errorLog;
        })
    }

    ibmWAAPI.prototype.deleteIntent = function(assistant, workspaceId, intent) {
        let deleteIntent = {
            intent: intent,
            workspace_id: workspaceId
        };
        // let assistant = new AssistantV1(watsonConfig);
        return new Promise((resolve, reject) => {
            assistant.deleteIntent(deleteIntent, (error, data) => {
                if(error) {
                    return reject(new Error(error));
                }
                return resolve(data);
            });
        }).catch((error) => {
            console.log(error);
            return null;
        })
    }

    ibmWAAPI.prototype.getEntity = function(assistant ,workspaceId, entity) {
        let getEntity = {
            entity: entity,
            workspace_id: workspaceId
        };
        // let assistant = new AssistantV1(watsonConfig);
        return new Promise((resolve, reject) => {
            assistant.getEntity(getEntity, (error, data) => {
                if(error) {
                    return reject(new Error(error));
                }
                return resolve(data);
            });
        }).catch((error) => {
            console.log(error);
            return null;
        })
    }

    ibmWAAPI.prototype.insertEntity = function(assistant, workspaceId, pushEntity) {
        pushEntity.workspace_id = workspaceId;
        // let assistant = new AssistantV1(watsonConfig);
        return new Promise((resolve, reject) => {
            assistant.createEntity(pushEntity, (error, data) => {
                if(error) {
                    return reject(new Error(error));
                }
                return resolve(pushEntity);
            });
        }).catch((error) => {
            console.log(error);
            let errorLog = {
                type: 'entity',
                action: 'insert',
                message: error.message,
                value: pushEntity.entity,
                timestamp: Date.now()
            }
            return errorLog;
        })
    }

    ibmWAAPI.prototype.putEntity = function(assistant, workspaceId, putEntity) {
        putEntity.workspace_id = workspaceId;
        // let assistant = new AssistantV1(watsonConfig);
        return new Promise((resolve, reject) => {
            assistant.updateEntity(putEntity, (error, data) => {
                if(error) {
                    return reject(new Error(error));
                }
                return resolve(putEntity);
            });
        }).catch((error) => {
            console.log(error);
            let errorLog = {
                type: 'entity',
                action: 'update',
                message: error.message,
                value: putEntity.entity,
                timestamp: Date.now()
            }
            return errorLog;
        })
    }

    ibmWAAPI.prototype.deleteEntity = function(assistant, workspaceId, entity) {
        let deleteEntity = {
            entity: entity,
            workspace_id: workspaceId
        };
        // let assistant = new AssistantV1(watsonConfig);
        return new Promise((resolve, reject) => {
            assistant.deleteEntity(deleteEntity, (error, data) => {
                if(error) {
                    return reject(new Error(error));
                }
                return resolve(data);
            });
        }).catch((error) => {
            console.log(error);
            return null;
        })
    }

    ibmWAAPI.prototype.getDialog = function(assistant, workspaceId, dialog) {
        let getDialog = {
            dialog_node: dialog,
            workspace_id: workspaceId
        };
        // let assistant = new AssistantV1(watsonConfig);
        return new Promise((resolve, reject) => {
            assistant.getDialogNode(getDialog, (error, data) => {
                if(error) {
                    return reject(new Error(error));
                }
                return resolve(data);
            });
        }).catch((error) => {
            console.log(error);
            return null;
        })
    }

    ibmWAAPI.prototype.insertDialog = function(assistant, workspaceId, pushDialog) {
        pushDialog.workspace_id = workspaceId;
        // let assistant = new AssistantV1(watsonConfig);
        return new Promise((resolve, reject) => {
            assistant.createDialogNode(pushDialog, (error, data) => {
                if(error) {
                    return reject(new Error(error));
                }
                return resolve(pushDialog);
            });
        }).catch((error) => {
            console.log(error);
            let errorLog = {
                type: 'dialog',
                action: 'insert',
                message: error.message,
                value: pushDialog.dialog_node,
                timestamp: Date.now()
            }
            return errorLog;
        })
    }

    ibmWAAPI.prototype.putDialog = function(assistant, workspaceId, putDialog) {
        putDialog.workspace_id = workspaceId;
        // let assistant = new AssistantV1(watsonConfig);
        return new Promise((resolve, reject) => {
            assistant.updateDialogNode(putDialog, (error, data) => {
                if(error) {
                    return reject(new Error(error));
                }
                return resolve(putDialog);
            });
        }).catch((error) => {
            console.log(error);
            let errorLog = {
                type: 'dialog',
                action: 'update',
                message: error.message,
                value: putDialog.dialog_node,
                timestamp: Date.now()
            }
            return errorLog;
        })
    }

    ibmWAAPI.prototype.deleteDialog = function(assistant, workspaceId, dialog) {
        let deleteDialog = {
            dialog: dialog,
            workspace_id: workspaceId
        };
        // let assistant = new AssistantV1(watsonConfig);
        return new Promise((resolve, reject) => {
            assistant.deleteDialogNode(deleteDialog, (error, data) => {
                if(error) {
                    return reject(new Error(error));
                }
                return resolve(data);
            });
        }).catch((error) => {
            console.log(error);
            return null;
        })
    }
    module.exports = new ibmWAAPI();
}())