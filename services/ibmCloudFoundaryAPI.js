(function() {
    const request = require('request-promise');
    const querystring = require('querystring');

    var baseUrl = '';

    function ibmCFAPI() {}

    ibmCFAPI.prototype.oauth = function(name, password, regionCode) {

        baseUrl = `https://api.${regionCode}.bluemix.net`;
        let url = baseUrl + '/v2/info';

        let endPoint = {
            'url': url,
            'method': 'GET'
        };

        return request.get(endPoint).then((body) => {
            let bodyJSON = JSON.parse(body);
            console.log(bodyJSON.authorization_endpoint);
    
            //Recogemos el endpoint necesario para obtener el token
            let oauth_tokenurl = bodyJSON.authorization_endpoint + "/oauth/token";
    
            //----------- params para el request ---------------
            let form = {
                "username": name,
                "password": password,
                "scope": "",
                "grant_type": "password"
            }
            let formData = querystring.stringify(form);
    
            //Variables petición Token
            let optionsToken = {
                url: oauth_tokenurl,
                method: 'POST',
                headers: {
                    "Accept-Encoding": "application/json",
                    "Content-Type": "application/x-www-form-urlencoded",
                    "Authorization": "Basic Y2Y6"
                },
                body: formData
            }
            return request.post(optionsToken);

        }).then((body) => {
            let bodyJSON = JSON.parse(body);
            return bodyJSON;
        }).catch((error) => {
            console.log('oauth error :' + error);
            return null;
        })
    }

    ibmCFAPI.prototype.getCloudantDBLite = function(access_token, token_type) {

        let url = baseUrl + '/v2/service_plans?q=unique_id:cloudant-lite';

        let endPoint = {
            'url': url,
            'method': 'GET',
            'headers': {
                "Authorization": token_type + " " + access_token
            }
        };

        return request.get(endPoint).then((body) => {
            let bodyJSON = JSON.parse(body);
            return bodyJSON;
        }).catch((error) => {
            console.log('getCloudantDBLite error: ' + error);
            return null;
        })
    }

    ibmCFAPI.prototype.getServiceInstance = function(access_token, token_type, instanceName, spaceGuid) {

        let url = baseUrl + `/v2/service_instances?q=name:${instanceName}`;

        if(spaceGuid) {
            url = baseUrl + `/v2/spaces/${spaceGuid}/service_instances?q=name:${instanceName}`;
        }

        let endPoint = {
            'url': url,
            'method': 'GET',
            'headers': {
                "Authorization": token_type + " " + access_token
            }
        };

        return request.get(endPoint).then((body) => {
            let bodyJSON = JSON.parse(body);
            return bodyJSON;
        }).catch((error) => {
            console.log('getServiceInstance error: ' + error);
            return null;
        })
    }

    ibmCFAPI.prototype.createCloudantDB = function(access_token, token_type, name, service_plan_guid, space_guid) {

        let url = baseUrl + '/v2/service_instances?accepts_incomplete=true';

        let body = {
            "name": name,
            "service_plan_guid": service_plan_guid,
            "space_guid": space_guid
        }

        let endPoint = {
            'url': url,
            'method': 'POST',
            'headers': {
                "Authorization": token_type + " " + access_token
            },
            'body': JSON.stringify(body)
        };

        return request.post(endPoint).then((body) => {
            let bodyJSON = JSON.parse(body);
            return bodyJSON;
        }).catch((error) => {
            console.log('createCloudantDB: ' + error);
            return null;
        })
    }

    ibmCFAPI.prototype.createServiceKey = function(access_token, token_type, name, serviceGuid) {

        let url = baseUrl + '/v2/service_keys';

        let body = {
            "name": name,
            "service_instance_guid": serviceGuid
        }

        let endPoint = {
            'url': url,
            'method': 'POST',
            'headers': {
                "Authorization": token_type + " " + access_token
            },
            'body': JSON.stringify(body)
        };

        return request.post(endPoint).then((body) => {
            let bodyJSON = JSON.parse(body);
            return bodyJSON;
        }).catch((error) => {
            console.log('createServiceKey: ' + error);
            return null;
        })
    }

    ibmCFAPI.prototype.getOrganizations = function(access_token, token_type, orgGuid) {

        let url = baseUrl + '/v2/organizations';

        if(orgGuid) {
            url = baseUrl + `/v2/organizations/${orgGuid}`;
        }

        let endPoint = {
            'url': url,
            'method': 'GET',
            'headers': {
                "Authorization": token_type + " " + access_token
            }
        };

        return request.get(endPoint).then((body) => {
            let bodyJSON = JSON.parse(body);
            return bodyJSON;
        }).catch((error) => {
            console.log('getOrganizations: ' + error);
            return null;
        })
    }

    ibmCFAPI.prototype.getSpaces = function(access_token, token_type, orgGuid) {

        let url = baseUrl + `/v2/organizations/${orgGuid}/spaces`;

        let endPoint = {
            'url': url,
            'method': 'GET',
            'headers': {
                "Authorization": token_type + " " + access_token
            }
        };

        return request.get(endPoint).then((body) => {
            let bodyJSON = JSON.parse(body);
            return bodyJSON;
        }).catch((error) => {
            console.log('getSpaces: ' + error);
            return null;
        })
    }

    ibmCFAPI.prototype.getServices = function(access_token, token_type, spaceGuid, nextPage) {

        let url = baseUrl + `/v2/spaces/${spaceGuid}/service_instances`;
        if(nextPage) {
            url = baseUrl + spaceGuid;
        }

        let endPoint = {
            'url': url,
            'method': 'GET',
            'headers': {
                "Authorization": token_type + " " + access_token
            }
        };

        return request.get(endPoint).then((body) => {
            let bodyJSON = JSON.parse(body);
            return bodyJSON;
        }).catch((error) => {
            console.log('getServices: ' + error);
            return null;
        })
    }

    ibmCFAPI.prototype.getServicekeys = function(access_token, token_type, serviceGuid) {

        let url = baseUrl + `/v2/service_instances/${serviceGuid}/service_keys`;

        let endPoint = {
            'url': url,
            'method': 'GET',
            'headers': {
                "Authorization": token_type + " " + access_token
            }
        };

        return request.get(endPoint).then((body) => {
            let bodyJSON = JSON.parse(body);
            return bodyJSON;
        }).catch((error) => {
            console.log('getServicekeys: ' + error);
            return null;
        })
    }

    ibmCFAPI.prototype.getUsage = function(access_token, token_type, regionCode, regionUrl, orgGuid, date) {

        let url = `https://rated-usage.${regionCode}.bluemix.net/v2/metering/organizations/${regionUrl}:${orgGuid}/usage/${date}`;

        let endPoint = {
            'url': url,
            'method': 'GET',
            'headers': {
                "Authorization": token_type + " " + access_token
            }
        };

        return request.get(endPoint).then((body) => {
            let bodyJSON = JSON.parse(body);
            console.log('get ' + date + ' usage');
            return bodyJSON;
        }).catch((error) => {
            console.log('getUsage: ' + error);
            return null;
        })
    }

    ibmCFAPI.prototype.direct = function(access_token, token_type, serviceInstantGuid) {
        
        let url = `https://watson-assistant.ng.bluemix.net/us-south/${serviceInstantGuid}/home`;
        let intentUrl = `https://assistant-us-south.watsonplatform.net/us-south/${serviceInstantGuid}/workspaces/bf4daff5-ed24-4f78-be5a-cc762934ffac/build/intents`

        let endPoint = {
            'url': intentUrl,
            'method': 'GET',
            'headers': {
                "Authorization": token_type + " " + access_token
            }
        };

        return request.get(endPoint).then((body) => {
            let bodyJSON = JSON.parse(body);
            return bodyJSON;
        }).catch((error) => {
            console.log('direct: ' + error);
            return null;
        })
    }

    module.exports = new ibmCFAPI();
}());