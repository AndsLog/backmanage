(function () {
    var body = JSON.parse(localStorage.getItem('ibmToken'));
    var worksapceNamesAndId = {};
    var file;
    var tooltipHeight;
    var tooltipWidth;
    var chartContainerWidth;
    var chartContainerHeight;
    var pieColor = {};

    var $clickedBtn;

    $(document).ready(function() {
        $('#username').text(body.username);
        fetchDailyUsage();
        getOrganizations();

        //Date picker
        $('.month-picker').datetimepicker({
            sideBySide: true,
            format:'YYYY-MM',
            locale: 'zh-tw'
        });

        $('.date-picker').datetimepicker({
            sideBySide: true,
            format: 'YYYY-MM-DD HH:mm',
            locale: 'zh-tw'
        });
    });

    

    $(document).on('click', '#get-conversation-history', getAIConversation);
    $(document).on('click', '#get-training-log', getTrainingLog);
    $(document).on('click', '#get-daily-usage', getDailyUsage);

    $(document).on('click', '#fetch-daily-usage', fetchDailyUsage);
    $(document).on('click', '#submit-file', formUpload);

    $(document).on('click', '#edit-intent', editIntent);
    $(document).on('click', '#intent-modal-btn', function(ev) {
        $clickedBtn = $(this);
        $('#select-intent').empty();
        let optionTags = $clickedBtn.attr('data-option');
        let targetSay = $clickedBtn.parent().parent().parent().find('#target-say').text()
        let intent = $clickedBtn.parent().parent().parent().find('#currently-Intent').text();
        let workspaceId = $clickedBtn.parent().parent().parent().attr('data-workspaceId');
        let docId = $clickedBtn.parent().parent().parent().attr('data-docId');
        $('#select-intent').attr('data-targetSay', targetSay);
        $('#select-intent').attr('data-CurrentlyIntent', intent);
        $('#select-intent').attr('data-workspaceId', workspaceId);
        $('#select-intent').attr('data-docId', docId);
        $('#select-intent').append(optionTags);
        $('.selectpicker').selectpicker('refresh');
    });

    $(document).on('change', '#organization', getSpaces);
    $(document).on('change', '#space', getServices);
    $(document).on('change', '#service', getWatsonList);
    $(document).on('change', '#watson-list', getWorkspaces);

    // $(document).on('click', '#organization li', function() {
    //     let text = $(this).text();
    //     $(this).addClass('org');
    //     $(this).siblings().removeClass('org');
        
    //     $('#org-text').val(text);
    //     getSpaces();
    // });
    // $(document).on('click', '#space li', function() {
    //     let text = $(this).text();
    //     $(this).addClass('space');
    //     $(this).siblings().removeClass('space');

    //     $('#space-text').val(text);
    //     getServices();
    // });
    // $(document).on('click', '#service li', function() {
    //     let text = $(this).text();
    //     $(this).addClass('service');
    //     $(this).siblings().removeClass('service');

    //     $('#service-text').val(text);
    //     getWatsonList();
    // });
    // $(document).on('click', '#watson-list li', function() {
    //     let text = $(this).text();
    //     $(this).addClass('watson');
    //     $(this).siblings().removeClass('watson');

    //     $('#watson-text').val(text);
    //     getWorkspaces();
    // });

    // AI訓練 選擇過律條件時，先還原城原本table
    $(document).on('click', 'input[name=filter-unit]:checked', function() {
        let checkedSearchType = $(this).val();

        $('#compare-operator').addClass('CSI-d-none');
        $('#confidence-level').addClass('CSI-d-none');

        $('.history-table-filter').removeClass('CSI-d-none');

        if (checkedSearchType === 'confidence') {
            $('#compare-operator').removeClass('CSI-d-none');
            $('#confidence-level').removeClass('CSI-d-none');

            $('.history-table-filter').addClass('CSI-d-none');
        }

        let $selectTrElements = $('#watson-history-list').children('tbody').children();
        $selectTrElements.each(function() {
            $(this).removeClass('CSI-d-none');
        });
        
    });

    // AI訓練 過濾處理
    $(document).on('click', '#filter-history', function() {
        let checkedSearchType = $('input[name=filter-unit]:checked').val();
        let operation = $('#compare-operator').find(':selected').attr('data-operation');
        let level =  parseFloat($('#confidence-level').find(':selected').attr('data-level'));
        let tableId = $(this).attr('data-table');
        let val = $('.history-table-filter').val().toLowerCase();
        if (checkedSearchType !== 'confidence') {
            let $selectTrElements = $('#' + tableId).children('tbody').children().find('#' + checkedSearchType);
            $selectTrElements.each(function() {
                let text = $(this).text().toLowerCase();
                $(this).parent().addClass('CSI-d-none');
                if(text.indexOf(val) >= 0) {
                    $(this).parent().removeClass('CSI-d-none');
                }
            });
        } else {
            let $selectTrElements = $('#' + tableId).children('tbody').children().find('#currently-Intent');
            $selectTrElements.each(function() {
                let confidenceValue = parseFloat($(this).attr('data-confidence'));
                $(this).parent().addClass('CSI-d-none');
                // 將符合條件的取消隱藏
                if (operation.indexOf('>') >= 0) {
                    if(confidenceValue > level) {
                        $(this).parent().removeClass('CSI-d-none');
                    }
                } else {
                    if(confidenceValue < level) {
                        $(this).parent().removeClass('CSI-d-none');
                    }
                }
            });
        }
    });

    // AI訓練明細 無過濾的值時，還原成原本的table
    $(document).on('change keyup', '.history-table-filter', function() {
        let checkedSearchType = $('input[name=filter-unit]:checked').val();
        let tableId = $(this).attr('data-table');
        let val = $(this).val().toLowerCase();
        if (checkedSearchType !== 'confidence' || !checkedSearchType) {
            if(!val) {
                let $selectTrElements = $('#' + tableId).children('tbody').children();
                $selectTrElements.each(function() {
                    $(this).removeClass('CSI-d-none');
                });
            }
        }
    });

    // AI訓練明細 過濾處理
    $(document).on('click', '#filter-log', function() {
        let tableId = $(this).attr('data-table');
        let val = $('.log-table-filter').val().toLowerCase();
        let $selectTrElements = $('#' + tableId).children('tbody').children();
        $selectTrElements.each(function() {
            let $selectTdElements = $(this).children();
            $(this).addClass('CSI-d-none'); // 先將全部tr隱藏
            $selectTdElements.each(function() {
                let text = $(this).text().toLowerCase();
                if(text.indexOf(val) >= 0) {
                    $(this).parent().removeClass('CSI-d-none'); // 當有一個td符合時就取消隱藏
                }
            });
        })
    });

    // AI訓練明細 無過濾的值時，還原城原本的table
    $(document).on('change keyup', '.log-table-filter', function() {
        let tableId = $(this).attr('data-table');
        let val = $(this).val().toLowerCase();
        if(!val) {
            let $selectTrElements = $('#' + tableId).children('tbody').children();
            $selectTrElements.each(function() {
                $(this).removeClass('CSI-d-none');
            });
        }
    });

    

    // 顯示 watson assistant管理頁面
    $(document).on('click', '#watson-admin', function() {
        $('#watson-admin-content').removeClass('CSI-d-none');
        $('#watson-admin-content').siblings().addClass('CSI-d-none');
        $(this).parent().addClass('active');
        $(this).parent().siblings().removeClass('active');
    });

    // 顯示 AI 訓練頁面
    $(document).on('click', '#AI-training', function() {
        $('#AI-training-content').removeClass('CSI-d-none');
        $('#AI-training-content').siblings().addClass('CSI-d-none');
        $(this).parent().addClass('active');
        $(this).parent().siblings().removeClass('active');
    });

    // 顯示 AI訓練明細頁面
    $(document).on('click', '#AI-training-log', function() {
        $('#AI-training-log-content').removeClass('CSI-d-none');
        $('#AI-training-log-content').siblings().addClass('CSI-d-none');
        $(this).parent().addClass('active');
        $(this).parent().siblings().removeClass('active');
    });

    // 顯示 花費頁面
    $(document).on('click', '#usage', function() {
        $('#usage-content').removeClass('CSI-d-none');
        $('#usage-content').siblings().addClass('CSI-d-none');
        $(this).parent().addClass('active');
        $(this).parent().siblings().removeClass('active');
    });

    // 顯示 表格上傳頁面
    $(document).on('click', '#file-import', function() {
        $('#file-import-content').removeClass('CSI-d-none');
        $('#file-import-content').siblings().addClass('CSI-d-none');
        $(this).parent().addClass('active');
        $(this).parent().siblings().removeClass('active');
    });

    $(document).on('change', '#upload-file', function() {
        let input = this;

        file = input.files[0];
        // input.value = '';
    });

    // 花費圖表操作 鼠標滑過 圓餅圖
    $(document).on('mouseover', '.pies', function(e) {
        $(this).css("z-index","2");
        let productName = $(this).attr("product-name");
        let pieTooltip = "<div id='tooltip' style='padding:5px;'><div style:'margin:0 auto;'>"+ productName +"</div></div>";
        $("body").append(pieTooltip);
        $("#tooltip").css({"top": (e.clientY+20) + "px","left": (e.clientX+10)  + "px"}).show();
    })

    // 花費圖表操作 鼠標滑出 圓餅圖
    $(document).on('mouseout', '.pies', function() {
        $("#tooltip").remove();
    });

    // 花費圖表操作 鼠標在 圓餅圖移動
    // 其中 top 的位置要由游標在整個頁面的 Y軸座標(e.pageY) 來計算
    $(document).on('mousemove', '.pies', function(e) {
        $("#tooltip").css({"left": (e.clientX + 20) + "px","top": (e.pageY - 20)  + "px" });
    });

    // 花費圖表操作 鼠標滑過 圓點
    $(document).on('mouseover', '.tips', function(e) {
        $(this).css("z-index","2");
        let usageDetial = $(this).attr("newtitle");
        let tooltip = "<div id='tooltip' style='padding:5px;'><div style:'margin:0 auto;'>"+ usageDetial +"</div></div>";
        $("body").append(tooltip);
        tooltipHeight = $("#tooltip").outerHeight();
        tooltipWidth = $("#tooltip").outerWidth();
        $("#tooltip").css({"top": (e.clientY+20) + "px","left": (e.clientX+10)  + "px"}).show();
    });
    

    // 花費圖表操作 鼠標滑出 圓點
    $(document).on('mouseout', '.tips', function() {
        $("#tooltip").remove();
    });

    // 花費圖表操作 鼠標在 圓點移動
    // 其中 top 的位置要由游標在整個頁面的 Y軸座標(e.pageY) 來計算
    $(document).on('mousemove', '.tips', function(e) {
        let tooltipX = e.clientX + tooltipHeight;
        let tooltipY = e.clientY + tooltipWidth;

        // 若表格寬超出視窗，將表格調整在游標的左側
        if(tooltipX >= window.innerWidth && tooltipY < window.innerHeight) {
            $("#tooltip").css({"left": (e.clientX - tooltipWidth - 20) + "px","top": (e.pageY- 20)  + "px"});
        
        // 若表格寬及高都超出視窗，將表格調整在游標的左上方
        } else if(tooltipX >= window.innerWidth && tooltipY >= window.innerHeight) {
            $("#tooltip").css({"left": (e.clientX - tooltipWidth - 20) + "px","top": (e.pageY - tooltipHeight - 20)  + "px"});
        
        // 若表格高超出視窗，將表格調整在游標的上方
        } else if(tooltipX < window.innerWidth && tooltipY >= window.innerHeight) {
            $("#tooltip").css({"left": (e.clientX - 20) + "px","top": (e.pageY  - tooltipHeight - 20)  + "px"});

        // 其餘情況，將表格放在游標的右側
        } else{
            $("#tooltip").css({"left": (e.clientX + 20) + "px","top": (e.pageY - 20)  + "px" });
        }
    });

    // watson assistant管理頁面 意圖 導頁
    $(document).on('click','#intent-link',function() {
        let regionUrl = body.regionUrl;
        let pagePath = $(this).attr('data-page_path');
        let workspaceId = $(this).parent().parent().parent().attr('data-workspaceId');
        let serviceGuid = $(this).parent().parent().parent().attr('data-service_guid');
        redirect(regionUrl, serviceGuid, workspaceId, pagePath);
    });

    // watson assistant管理頁面 實體 導頁
    $(document).on('click','#entity-link',function() {
        let regionUrl = body.regionUrl;
        let pagePath = $(this).attr('data-page_path');
        let workspaceId = $(this).parent().parent().parent().attr('data-workspaceId');
        let serviceGuid = $(this).parent().parent().parent().attr('data-service_guid');
        redirect(regionUrl, serviceGuid, workspaceId, pagePath);
    });

    // watson assistant管理頁面 對話 導頁
    $(document).on('click','#dialog-link',function() {
        let regionUrl = body.regionUrl;
        let pagePath = $(this).attr('data-page_path');
        let workspaceId = $(this).parent().parent().parent().attr('data-workspaceId');
        let serviceGuid = $(this).parent().parent().parent().attr('data-service_guid');
        redirect(regionUrl, serviceGuid, workspaceId, pagePath);
    });

    // watson assistant管理頁面 數據總覽 導頁
    $(document).on('click','#overview-link',function() {
        let regionUrl = body.regionUrl;
        let pagePath = $(this).attr('data-page_path');
        let workspaceId = $(this).parent().parent().parent().attr('data-workspaceId');
        let serviceGuid = $(this).parent().parent().parent().attr('data-service_guid');
        redirect(regionUrl, serviceGuid, workspaceId, pagePath);
    });

    function redirect(regionUrl, serviceGuid, workspaceId, pagePath) {
        let url = `https://assistant-${regionUrl}.watsonplatform.net/${regionUrl}/${serviceGuid}/workspaces/${workspaceId}/${pagePath}`
        window.open(url);
    }

    function getOrganizations() {
        $('body').addClass('CSI-wait');
        $('#org-text').val();
        return callAPI('/backstage/organizations', 'POST', body).then((resJson) => {
            $('#organization').empty();
            let organizations = resJson.resources;
            let selectTag = '';
            organizations.map((organization, i) => {
                
                selectTag += `<option data-organization_guid="${organization.metadata.guid}" data-qutoa_guid="${organization.entity.quota_definition_guid}">${organization.entity.name}</option>`;
                // if (i === 0) {
                //     selectTag += `<li class="org" data-organization_guid="${organization.metadata.guid}" data-qutoa_guid="${organization.entity.quota_definition_guid}"><a>${organization.entity.name}</a></li>`;
                //     $('#org-text').val(organization.entity.name);
                //     return;
                // }
                // selectTag += `<li data-organization_guid="${organization.metadata.guid}" data-qutoa_guid="${organization.entity.quota_definition_guid}"><a>${organization.entity.name}</a></li>`;
            });
            $('#organization').append(selectTag);        
            console.log(resJson);
        }).then(() => {
            $('body').removeClass('CSI-wait');
            getSpaces()
        }).catch((error) => {
            $('body').removeClass('CSI-wait');
            console.log(error);
        });
    }
    
    function getSpaces() {
        $('body').addClass('CSI-wait');
        $('#space-text').val('');
        let organizationGuid = $('#organization').find(':selected').attr('data-organization_guid');
        // let organizationGuid = $('#organization').find('.org').attr('data-organization_guid');
        return callAPI(`/backstage/spaces/${organizationGuid}`, 'POST', body).then((resJson) => {
            $('#space').empty();
            let spaces = resJson.resources;
            let selectTag = '';
            spaces.map((space, i) => {
                let spaceGuid = space.metadata.guid;
                let entityName = space.entity.name;
                
                selectTag += `<option data-space_guid="${spaceGuid}">${entityName}</option>`;
                // if(i === 0) {
                //     selectTag += `<li class="space" data-space_guid="${spaceGuid}"><a>${entityName}</a></li>`;
                //     $('#space-text').val(entityName);
                //     return;
                // }
                // selectTag += `<li data-space_guid="${spaceGuid}"><a>${entityName}</a></li>`;
            });
            $('#space').append(selectTag);
            console.log(resJson);
        }).then(() => {
            $('body').removeClass('CSI-wait');
            getServices()
        }).catch((error) => {
            $('body').removeClass('CSI-wait');
            console.log(error);
        });
    }
    
    function getServices() {
        $('body').addClass('CSI-wait');
        $('#service-text').val('');
        let spaceGuid = $('#space').find(':selected').attr('data-space_guid');
        // let spaceGuid = $('#space').find('.space').attr('data-space_guid');
        return callAPI(`/backstage/services/${spaceGuid}`, 'POST', body).then((resJson) => {
            $('#service').empty();
            let services = resJson.resources;
            let selectTag = '';
            let i = 0;
            services.map((service) => {
                let serviceGuid = service.metadata.guid;
                let entityName = service.entity.name;
                let dashboardUrl = service.entity.dashboard_url;
                if(dashboardUrl.indexOf('cloudant') !== -1 || entityName.indexOf('cloudant') !== -1) {
        
                    selectTag += `<option data-service_guid="${serviceGuid}">${entityName}</option>`;
                }
                selectTag += `<option class="CSI-d-none" data-service_guid="${serviceGuid}">${entityName}</option>`;
                // if(dashboardUrl.indexOf('cloudant') !== -1 || entityName.indexOf('cloudant') !== -1) {
                //     if(i === 0) {
                //         selectTag += `<li class="service" data-service_guid="${serviceGuid}"><a>${entityName}</a></li>`;
                //         $('#service-text').val(entityName);
                //         i++;
                //         return;
                //     }
                //     selectTag += `<li data-service_guid="${serviceGuid}"><a>${entityName}</a></li>`;
                // }
                // selectTag += `<li class="CSI-d-none" data-service_guid="${serviceGuid}"><a>${entityName}</a></li>`;
            });
            $('#service').append(selectTag);
            console.log(resJson);
        }).then(() => {
            $('body').removeClass('CSI-wait');
            getWatsonList();
        }).catch((error) => {
            $('body').removeClass('CSI-wait');
            console.log(error);
        });
    }

    function getWatsonList() {
        $('body').addClass('CSI-wait');
        $('#watson-text').val('');
        let serviceGuid = $('#service').find(':selected').attr('data-service_guid');
        // let serviceGuid = $('#service').find('.service').attr('data-service_guid');
        return callAPI(`/backstage/watsonassistant/services/${serviceGuid}`, 'POST', body).then((resJson) => {
            $('#watson-list').empty();
            if(!resJson) {
                return;
            }

            let watsons = resJson.rows;
            let selectTag = '';
            watsons.map((watson, i) => {
                
                selectTag += `<option data-watsonName="${watson.key.watsonName}">${watson.key.watsonName}</option>`;
                // if(i === 0) {
                //     selectTag += `<li class="watson" data-watsonName="${watson.key.watsonName}"><a>${watson.key.watsonName}</a></li>`;
                //     $('#watson-text').val(watson.key.watsonName);
                //     return;
                // }
                // selectTag += `<li data-watsonName="${watson.key.watsonName}"><a>${watson.key.watsonName}</a></li>`;
            });
            $('#watson-list').append(selectTag);
            console.log(resJson);
        }).then(() => {
            $('body').removeClass('CSI-wait');
            getWorkspaces();
        }).catch((error) => {
            $('body').removeClass('CSI-wait');
            console.log(error);
        });
    }

    function getWorkspaces() {
        $('body').addClass('CSI-wait');
        $('#watson-admin-body').addClass('CSI-d-none');
        let watsonName = $('#watson-list').find(':selected').attr('data-watsonName');
        // let watsonName = $('#watson-list').find('.watson').attr('data-watsonName');
        return callAPI(`/backstage/watsonassistant/${watsonName}/workspace`, 'POST', body).then((resJson) => {           
            $('#workspace-list').empty();
            $('#workspace-pane').empty();
            $('#file-upload-workspaces').empty();

            worksapceNamesAndId = {};
            let workspaces = resJson.workspaces;
            let serviceGuid = resJson.serviceGuid;
            workspaces.map((workspace) => {
                let workspaceName = workspace.name;
                let trimWorkspaceName = trim(workspaceName);
                let workspaceId = workspace.workspace_id;
                let liTag = `<li id="workspace-li"><a href="#${trimWorkspaceName}" data-toggle="tab">${workspaceName}</a></li>`;
                let divTag = `<div class="tab-pane" id="${trimWorkspaceName}" data-workspaceId="${workspaceId}" data-service_guid="${serviceGuid}">
                                    <div class="CSI-list-style">
                                        <div class="row">
                                            <button id="intent-link" data-page_path="build/intents" class="CSI-btn-style btn-info btn-flat" type="button">意圖(Intents)</button>
                                            <button id="entity-link" data-page_path="build/entities/user" class="CSI-btn-style btn-info btn-flat" type="button">實體(Entities)</button>
                                        </div>

                                        <div class="row">
                                            <button id="dialog-link" data-page_path="build/dialog" class="CSI-btn-style btn-info btn-flat" type="button">對話(Dialogs)</button>
                                            <button id="overview-link" data-page_path="improve/overview" class="CSI-btn-style btn-info btn-flat" type="button">數據總覽</button>
                                        </div>
                                    </div>
                                </div>`;

                let optionTag = `<option data-workspaceId="${workspaceId}" data-service_guid="${serviceGuid}" value="${workspaceName}">${workspaceName}</option>`
                $('#workspace-list').append(liTag);
                $('#workspace-pane').append(divTag);

                $('#file-upload-workspaces').append(optionTag);
                worksapceNamesAndId[workspaceId] = trimWorkspaceName;
            })
            let worksapceNamesAndIdKey = Object.keys(worksapceNamesAndId)[0];
            let firstWorkspaceName = worksapceNamesAndId[worksapceNamesAndIdKey];
            $(`a[href="#${firstWorkspaceName}"]`).parent().addClass('active');
            $(`#${firstWorkspaceName}`).addClass('active');
        }).then(() => {
            $('#watson-admin-body').removeClass('CSI-d-none');
            $('body').removeClass('CSI-wait');
        }).catch((error) => {
            $('body').removeClass('CSI-wait');
            console.log(error);
        })
    }
    
    function getAIConversation() {
        $('body').addClass('CSI-wait');
        $('#AI-training-body').addClass('CSI-d-none');
        let body = JSON.parse(localStorage.getItem('ibmToken'));
        let serviceGuid = $('#service').find(':selected').attr('data-service_guid');
        // let serviceGuid = $('#service').find('.service').attr('data-service_guid');
        let watsonName = $('#watson-list').find(':selected').attr('data-watsonName');
        // let watsonName = $('#watson-list').find('.watson').attr('data-watsonName');
        let startTime = Date.parse($('#start-time').val());
        let endTime = Date.parse($('#end-time').val());
        body.startTime = startTime;
        body.endTime = endTime;
        return callAPI(`/backstage/watsonassistant/${watsonName}/say/${serviceGuid}`, 'POST', body).then((resJson) => {
            $('#watson-history-list').DataTable().destroy();
            $('#show-converstaion-history').empty();
            let resJsonLength = Object.keys(resJson).length;
            if (0 === resJsonLength || !resJson.docs) {
                return;
            }
            let conversations = resJson.docs;
            let workspaceIntentList = resJson.workspaceIntentList;
            let workspaceId = resJson.watsonWorkSpaceId;
            conversations.map((conversation) => {
                let docId = conversation._id;

                let time = new Date(conversation.timestamp);
                let localTime = time.toLocaleString();
                let targetSay = conversation.input.text;
                let watsonSay = conversation.output.text[0];
                let intentList = workspaceIntentList[workspaceId] === undefined ? [] : workspaceIntentList[workspaceId].intents.intents ;
                let trTag = '';
                let optionTag = '';
                let originIntents = conversation.intents || '';
                let updateIntent = conversation.updateIntent || '';
                let intent;
                if (updateIntent) {
                    intent = updateIntent;
                    trTag += `<tr style="background-color:#f3cbe5;" id="watson-say-lists" data-docId="${docId}" data-workspaceId="${workspaceId}">
                                <td class="col-3" data-sort="${conversation.timestamp}">${localTime}</td>
                                <td class="col-3" title="${worksapceNamesAndId[workspaceId]}">${worksapceNamesAndId[workspaceId]}</td>
                                <td class="col-3" id="target-say" title="${targetSay}">${targetSay}</td>
                                <td class="col-3" id="currently-Intent" title="${updateIntent}">${updateIntent}</td>`;
                } else {
                    if(originIntents.length === 0) {
                        trTag += `<tr id="watson-say-lists" data-docId="${docId}" data-workspaceId="${workspaceId}">
                                    <td class="col-3" data-sort="${conversation.timestamp}">${localTime}</td>
                                    <td class="col-3" title="${worksapceNamesAndId[workspaceId]}">${worksapceNamesAndId[workspaceId]}</td>
                                    <td class="col-3" id="target-say" title="${targetSay}">${targetSay}</td>
                                    <td class="col-3" id="currently-Intent" title="無">無</td>`;
                    } else{
                        originIntents.map((originIntent) => {
                            intent = originIntent.intent;
                            let confidenceValue = originIntent.confidence;
                            trTag += `<tr id="watson-say-lists" data-docId="${docId}" data-workspaceId="${workspaceId}">
                                        <td class="col-3" data-sort="${conversation.timestamp}">${localTime}</td>
                                        <td class="col-3" title="${worksapceNamesAndId[workspaceId]}">${worksapceNamesAndId[workspaceId]}</td>
                                        <td class="col-3" id="target-say" title="${targetSay}">${targetSay}</td>
                                        <td class="col-3" id="currently-Intent" data-confidence="${confidenceValue}" title="${intent}">${intent}</td>`;
                        });
                    }
                }

                if(intentList.length) {
                  intentList.map((watsonIntent) => {
                      if(intent === watsonIntent.intent) {
                          optionTag = `<option>${watsonIntent.intent}</option>` + optionTag;
                      } else {
                        optionTag += `<option>${watsonIntent.intent}</option>`;
                      }
                  });
                }
              
                optionTag +=`<option>irrelevant</option>`;
                trTag += `<td class="col-3" id="button-operation">
                          <div class="col">
                              <button type="button" id="intent-modal-btn" class="btn btn-primary" data-target="#edit-intent-modal" data-toggle="modal" data-option="${optionTag}">修改</button>
                          </div>
                        </td>
                        </tr>`;
                $('#show-converstaion-history').append(trTag);
            });
        }).then(() => {
            $('#watson-history-list').DataTable({
                destroy: true,
                searching: false,
                autoWidth: false,
                pageLength: 50,
                order: [[ 0, "desc" ]],
                columnDefs: [{
                    targets: [4],
                    orderable: false,
                }],
                oLanguage: {
                    sProcessing: "處理中...",
                    sLengthMenu: "顯示 _MENU_ 項結果",
                    sZeroRecords: "沒有匹配結果",
                    sInfo: "顯示第 _START_ 至 _END_ 項結果，共 _TOTAL_ 項",
                    sInfoEmpty: "顯示第 0 至 0 項結果，共 0 項",
                    sInfoFiltered: "(從 _MAX_ 項結果過濾)",
                    sSearch: "搜索:",
                    oPaginate:{
                        sFirst: "首頁",
                        sPrevious: "上頁",
                        sNext: "下頁",
                        sLast: "尾頁"
                    }
                }
            });
        }).then(() => {
            $('#AI-training-body').removeClass('CSI-d-none');
            $('body').removeClass('CSI-wait');
        }).catch((error) => {
            $('body').removeClass('CSI-wait');
            console.log(error);
        });
    }

    function editIntent() {
        $('body').addClass('CSI-wait');
        let body = JSON.parse(localStorage.getItem('ibmToken'));
        // let text = $(this).parent().parent().parent().find('#target-say').text()
        // let intent = $(this).parent().parent().parent().find('#currently-Intent').text();
        // let updateIntent = $(this).parent().parent().find('#edited-intent').val();
        let text = $('#edit-intent-modal').find('#select-intent').attr('data-targetSay');
        let intent = $('#edit-intent-modal').find('#select-intent').attr('data-currentlyIntent');
        let updateIntent = $('#edit-intent-modal').find('#select-intent').val();
        let serviceGuid = $('#service').find(':selected').attr('data-service_guid');
        // let serviceGuid = $('#service').find('.service').attr('data-service_guid');
        let watsonName = $('#watson-list').find(':selected').attr('data-watsonName');
        // let watsonName = $('#watson-list').find('.watson').attr('data-watsonName');
        // let workspaceId = $(this).parent().parent().parent().attr('data-workspaceId');
        // let docId = $(this).parent().parent().parent().attr('data-docId');
        let workspaceId = $('#edit-intent-modal').find('#select-intent').attr('data-workspaceId');
        let docId = $('#edit-intent-modal').find('#select-intent').attr('data-docId');

        if (intent === updateIntent) {
            alert('意圖相同');
            return;
        }
        body.updateIntent = updateIntent;
        body.intent = intent;
        body.text = text;
        body.docId = docId;
        return callAPI(`/backstage/watsonassistant/${watsonName}/workspace/${workspaceId}/services/${serviceGuid}/push`, 'POST', body).then((resJson) => {
            let updateIntent = resJson.docs[0].updateIntent;
            $clickedBtn.parent().parent().parent().find('#currently-Intent').text(updateIntent);
            $clickedBtn.parent().parent().parent().css('background-color', '#f3cbe5')
            $('body').removeClass('CSI-wait');
            $('#edit-intent-modal').modal('hide');
            console.log(resJson);
        }).catch((error) => {
            $('body').removeClass('CSI-wait');
            console.log(error);
        });
    }

    function getTrainingLog() {
        $('body').addClass('CSI-wait');
        $('#AI-training-log-body').addClass('CSI-d-none');
        let body = JSON.parse(localStorage.getItem('ibmToken'));
        let serviceGuid = $('#service').find(':selected').attr('data-service_guid');
        // let serviceGuid = $('#service').find('.service').attr('data-service_guid');
        let watsonName = $('#watson-list').find(':selected').attr('data-watsonName');
        // let watsonName = $('#watson-list').find('.watson').attr('data-watsonName');
        let startTime = new Date($('#training-log-start-time').val()).getTime();
        let endTime = new Date($('#training-log-end-time').val()).getTime();
        body.startTime = startTime;
        body.endTime = endTime;
        return callAPI(`/backstage/watsonassistant/${watsonName}/log/${serviceGuid}`, 'POST', body).then((resJson) => {
            $('#training-log-list').DataTable().destroy();
            $('#show-training-log').empty();
            let resJsonLength = Object.keys(resJson).length;
            if(0 === resJsonLength || !resJson.docs) {
                return;
            }
            let trainingLogs = resJson.docs;
            trainingLogs.map((trainingLog) => {
                let time = new Date(trainingLog.timestamp);
                let localTime = time.toLocaleString();
                let text = trainingLog.example;
                let workspaceId = trainingLog.workspaceId;
                let worsapceName = worksapceNamesAndId[workspaceId] || '未記錄'
                let originIntent = trainingLog.intent;
                let updateIntent = trainingLog.updateIntent || '未記錄';
                let username = trainingLog.username || '未記錄';
                let trTag = `<tr>
                                <td data-sort="${trainingLog.timestamp}">${localTime}</td>
                                <td title="${worsapceName}">${worsapceName}</td>
                                <td title="${text}">${text}</td>
                                <td title="${originIntent}">${originIntent}</td>
                                <td title="${updateIntent}">${updateIntent}</td>
                                <td title="${username}">${username}</td>
                            </tr>`;
                $('#show-training-log').append(trTag);
            });
        }).then(() => {
            $('#training-log-list').DataTable({
                destroy: true,
                searching: false,
                autoWidth: false,
                pageLength: 50,
                order: [[ 0, "desc" ]],
                oLanguage: {
                    sProcessing: "處理中...",
                    sLengthMenu: "顯示 _MENU_ 項結果",
                    sZeroRecords: "沒有匹配結果",
                    sInfo: "顯示第 _START_ 至 _END_ 項結果，共 _TOTAL_ 項",
                    sInfoEmpty: "顯示第 0 至 0 項結果，共 0 項",
                    sInfoFiltered: "(從 _MAX_ 項結果過濾)",
                    sSearch: "搜索:",
                    oPaginate:{
                        sFirst: "首頁",
                        sPrevious: "上頁",
                        sNext: "下頁",
                        sLast: "尾頁"
                    }
                }
            });
        }).then(() => {
            $('#AI-training-log-body').removeClass('CSI-d-none');
            $('body').removeClass('CSI-wait');
        }).catch((error) => {
            $('body').removeClass('CSI-wait');
            console.log(error);
        });
    }

    function getDailyUsage() {
        $('body').addClass('CSI-wait');
        $('#usage-line-chart').empty();
        $('#usage-pie-chart').empty();
        $('#usage-list').empty();
        $('#usage-pane').empty();
        $('#usage-table').empty();
        $('#usage-body').addClass('CSI-d-none');
        let body = JSON.parse(localStorage.getItem('ibmToken'));
        let organizationGuid = $('#organization').find(':selected').attr('data-organization_guid');
        // let organizationGuid = $('#organization').find('.org').attr('data-organization_guid');
        let spaceGuid = $('#space').find(':selected').attr('data-space_guid');
        // let spaceGuid = $('#space').find('.space').attr('data-space_guid');
        let date = $('#time-input').val(); //e.g. :2018-8
        let year = date.substring(0, 4); //e.g. :2018
        let nextMonth = parseInt(date.substring(date.length-1, date.length)) + 1; //e.g. :9
        let selectedYearMonth = `${year}-${nextMonth}`; //e.g. :2018-9
        let unixSelectedMonth = new Date(date).setHours(0, 0, 0, 0); // 時間設為凌晨零點
        let unixSelectedNextMonth = new Date(selectedYearMonth).setHours(0, 0, 0, 0); // 時間設為凌晨零點
        let unixLastDayofSelectedMonth = unixSelectedNextMonth - 1000; //下個月的第一天凌晨0點 減 一秒就是當月最後一天
        body.firstDay = unixSelectedMonth;
        body.lastDay = unixLastDayofSelectedMonth;
        body.type = 'daily';
        return callAPI(`/backstage/usage/daily/org/${organizationGuid}/space/${spaceGuid}`, 'POST', body).then((resJson) => {
            let resJsonLength = Object.keys(resJson).length;
            let liTag = `<li class="active"><a href="#daily" data-toggle="tab">每日預估</a></li>`;
            let divTag = `<div class="tab-pane active" id="daily">
                            <div id="usage-line-chart"></div>
                        </div>`;
            if(0 === resJsonLength) {
                divTag = `<div class="tab-pane active" id="daily">
                            <div id="usage-line-chart">尚無相關資料</div>
                        </div>`;
                $('#usage-list').append(liTag);
                $('#usage-pane').append(divTag);
            } else {
                let dailyUsages = resJson.docs;
                let dailyUsageCosts = countUsage(dailyUsages);
                $('#usage-list').append(liTag);
                $('#usage-pane').append(divTag);
                drawLine('usage-line-chart', dailyUsageCosts);
            }
        }).then(() => {
            body.type = 'monthly';
            return callAPI(`/backstage/usage/daily/org/${organizationGuid}/space/${spaceGuid}`, 'POST', body);
        }).then((resJson) => {
            let resJsonLength = Object.keys(resJson).length;
            let liTag = `<li ><a href="#monthly" data-toggle="tab">月結算</a></li>`;
            let divTag = `<div class="tab-pane" id="monthly">
                            <div class="row">
                                <div class="col-md-6" id="usage-pie-chart"></div>
                                <div class="col-md-6" id="usage-table"></div>
                            </div>
                        </div>`;
            if(0 === resJsonLength) {
                divTag = `<div class="tab-pane" id="monthly">
                            <div id="usage-pie-chart">尚無相關資料</div>
                        </div>`;
                $('#usage-list').append(liTag);
                $('#usage-pane').append(divTag);
            } else {
                let monthlyUsages = resJson.docs;
                let monthlyUsageCosts = countUsage(monthlyUsages);
                $('#usage-list').append(liTag);
                $('#usage-pane').append(divTag);
                drawPie('usage-pie-chart', monthlyUsageCosts);
                appendtable('usage-table', monthlyUsageCosts);
            }
            return;
        }).then(() => {
            $('body').removeClass('CSI-wait');
            $('#usage-body').removeClass('CSI-d-none');
        }).catch((error) => {
            $('body').removeClass('CSI-wait');
            console.log(error);
        });
    }

    function countUsage(usages) {
        let usageArray = [];
        usages.map((usage) => {
            let productUsages = [];
            let dailyTotalUsage = 0;
            let products = usage.products
            let timestamp = new Date(usage.timestamp);
            let productIds = Object.keys(products);
            productIds.map((productId) => {
                let productAllCost = 0;
                let items = products[productId].items;
                let itemIds = Object.keys(items);
                itemIds.map((itemId) => {
                    let usages = products[productId].items[itemId].usage;
                    let usageIds = Object.keys(usages);
                    usageIds.map((usageId) => {
                        productAllCost += products[productId].items[itemId].usage[usageId].cost;
                    })
                })
                if(productAllCost) {
                    let productName = productId;
                    if(products[productId].name) {
                        productName = products[productId].name;
                    }
                    productUsages.push({
                        name: productName,
                        cost: Math.round(productAllCost*100)/100
                    })
                    dailyTotalUsage += Math.round(productAllCost*100)/100;
                }
            });
            usageArray.push({
                'date': timestamp,
                'dailyTotalUsage': Math.round(dailyTotalUsage*100)/100,
                'productUsages': productUsages
            });
        });
        return usageArray;
    }

    function fetchDailyUsage() {
        let body = JSON.parse(localStorage.getItem('ibmToken'));
        return callAPI(`/backstage/usage`, 'POST', body).then((resJson) => {
            console.log(resJson);
        }).catch((error) => {
            console.log(error);
        });
    }

    function appendtable(area, data) {
        let loaclDate = data[0].date;
        let dailyTotalUsage = data[0].dailyTotalUsage;
        let productUsages = data[0].productUsages;
        let tableTag = `<table id="${loaclDate}" class="table table-bordered">
                        <tr>
                            <th>類型</th>
                            <th>花費</th>
                            <th>百分比</th>
                        </tr>`;
        productUsages.map((productUsage) => {
            let percent = Math.round(productUsage.cost/dailyTotalUsage*10000)/100;
            tableTag += `<tr>
                            <td><div class="pie-color-block" style="background-color:${pieColor[productUsage.name]};"></div> ${productUsage.name}</td>
                            <td>${productUsage.cost}</td>
                            <td>${percent}%</td>
                        </tr>`;
            
        })
        tableTag += '</table>';
        $('#' + area).append(tableTag);
    }

    function drawLine(drawArea, items) {
        $('#' + drawArea).empty();

        chartContainerWidth = $('#usage-content').width()*9/10;
        chartContainerHeight = $('#usage-content').height()*2;

        var margin = {top: 20, right: 20, bottom: 60, left: 50};
        var width = chartContainerWidth - margin.left - margin.right;
        var height = chartContainerHeight - margin.top - margin.bottom;

        // set the ranges
	    var x = d3.scaleTime().range([0, width]);
        var y = d3.scaleLinear().range([height, 0]);
        
        // define the line
        var valueline = d3.line()
                        .x(function(d) { return x(d.date); })
                        .y(function(d) { return y(d.dailyTotalUsage); });

        // append the svg obgect to the body of the page
        // appends a 'group' element to 'svg'
        // moves the 'group' element to the top left margin
        var svg = d3.select("#" + drawArea).append("svg")
                    .attr("width", width + margin.left + margin.right)
                    .attr("height", height + margin.top + margin.bottom)
                .append("g")
                    .attr("transform",
                        "translate(" + margin.left + "," + margin.top + ")");
        // format the data
        items.forEach(function(d) {
            d.date = new Date(d.date.setHours(0, 0, 0, 0));
            d.dailyTotalUsage = d.dailyTotalUsage;
        });

        // Scale the range of the data
	    x.domain(d3.extent(items, function(d) { return d.date; }));
        y.domain([0, d3.max(items, function(d) { return d.dailyTotalUsage; })]);
        
        // Add the valueline path.
        svg.append("path")
            .data([items])
            .attr("class", "line-blue")
            .attr("d", valueline);

        // Add the X Axis
        let xAxis_scale = d3.axisBottom(x).ticks(items.length).tickFormat(d3.timeFormat("%m月%d日"));
        svg.append("g")
            .attr("transform", "translate(0," + height + ")")
            .call(xAxis_scale)
            .selectAll("text")	
              .style("text-anchor", "end")
              .attr("dx", "-.8em")
              .attr("dy", ".15em")
              .attr("transform", "rotate(-35)");

        // Add the Y Axis
	    svg.append("g")
            .call(d3.axisLeft(y));

        // Add cicrcle point
        svg.append("g").selectAll("circle")
            .data(items)
            .enter()
            .append("circle")
            .attr("cx",function(d) {return x(d.date);})
            .attr("cy",function(d) {return y(d.dailyTotalUsage);})
            .attr("r" , 6)
            .attr("fill", "rgba(0,0,0)")
            .attr("newtitle" , function(data){
                let productUsages = data.productUsages;
                let productIds = Object.keys(productUsages);
                let date = data.date.getDate() < 10 ? '0' + data.date.getDate() :  data.date.getDate() ;
                let month = (data.date.getMonth() + 1) < 10 ? '0' + (data.date.getMonth() + 1) : (data.date.getMonth() + 1);
                let usageDetial = `<div class="box">
                                        <div class="box-header with-border">
                                            <h3 class="box-header with-border">${month}月${date}日</h3>
                                        </div>
                                        <div class="box-body">
                                            <table class="table table-bordered">
                                            <tbody><tr>
                                                    <th>類型</th>
                                                    <th>花費</th>
                                                </tr>`;
                productUsages.map((productUsage) => {
                    usageDetial += `<tr>
                                        <td>${productUsage.name}</td>
                                        <td>${productUsage.cost}</td>
                                    </tr>`;
                })
                usageDetial += '</tbody></table></div></div>';
                return usageDetial;
            })
            .attr("class","tips");
    }

    function drawPie(drawArea, items) {
        $('#' + drawArea).empty();

        var productUsages = items[0].productUsages;
        var dailyTotalUsage = items[0].dailyTotalUsage;
        var productNum = productUsages.length;

        var width = chartContainerWidth/3;
        var height = chartContainerHeight/2;
        var radius = Math.min(width, height) / 2;
        var labelr = radius + 20;

        var color = d3.scaleOrdinal(d3.schemeCategory10);

        var arc = d3.arc()
            .outerRadius(radius - 5)
            .innerRadius(0);

        var labelArc = d3.arc()
            .outerRadius(radius - 10)
            .innerRadius(radius - 10);

        var pie = d3.pie()
            .sort(null)
            .value(function(d) {
                return d.cost;
            });

        //Create SVG element
        var svg = d3.select("#" + drawArea).append("svg")
            .attr("width", width)
            .attr("height", height)
       
        //Set up groups
        var arcs = svg.selectAll("g.arc")
                        .data(pie(productUsages))
                        .enter()
                        .append("g")
                        .attr("class", "arc")
                        .attr("transform", "translate(" +  width/2 + "," + height/2 + ")");
        
        //Draw arc paths
        arcs.append("path")
            .attr("d", arc)
            .style("fill", function(d, i) {
                pieColor[d.data.name] = color(i);
                return color(i);
            })
            .attr("product-name", function(d, i) {
                let percent = Math.round(d.data.cost/dailyTotalUsage*10000)/100;
                let productUsage = `類型: ${d.data.name}<br>
                                    花費: ${d.data.cost}<br>
                                    百分比: ${percent}%`;
                return productUsage;
            })
            .attr("class", "pies");
    }

    function formUpload(){
        $('body').addClass('CSI-wait');
        let body = JSON.parse(localStorage.getItem('ibmToken'));
        let workspaceId = $('#file-upload-workspaces').find(':selected').attr('data-workspaceid');
        let watsonGuid = $('#file-upload-workspaces').find(':selected').attr('data-service_guid');
        let DBserviceGuid = $('#service').find(':selected').attr('data-service_guid');
        // let DBserviceGuid = $('#service').find('.service').attr('data-service_guid');

        let rABS = true; // true: readAsBinaryString ; false: readAsArrayBuffer

        let reader = new FileReader();
        reader.onload = function(e) {
            let data = e.target.result;
            if(!rABS) {
                data = new Uint8Array(data);
            }
            let workbook = XLSX.read(data, {type: rABS ? 'binary' : 'array'});
            let sheetNames = workbook.SheetNames;
            let intentAndDialogWorksheet = workbook.Sheets[sheetNames[0]];
            let entityWorksheet = workbook.Sheets[sheetNames[1]];
            let intentAndDialogJson = XLSX.utils.sheet_to_json(intentAndDialogWorksheet);
            let entityJson = XLSX.utils.sheet_to_json(entityWorksheet);
            let jsonData = {
                intentsAndDialogs: intentAndDialogJson,
                entities: entityJson
            }
            body.uploadData = jsonData;
            return callAPI(`/backstage/watsonassistant/${watsonGuid}/workspace/${workspaceId}/services/${DBserviceGuid}/uploadfile`, 'POST', body).then((resJson) => {
                $('body').removeClass('CSI-wait');
                let uploadResults = resJson;
                let failText = '';
                uploadResults.map((uploadResult) => {
                    if(!uploadResult.result) {
                        failText += `${uploadResult.type} : ${uploadResult.value}  ${uploadResult.message} \n`;
                    }
                })
                if(!failText) {
                    alert('上傳成功');
                } else {
                    alert(failText);
                }
            }).catch((error) => {
                $('body').removeClass('CSI-wait');
                console.log(error);
                alert('上傳失敗');
            })
        
            /* DO SOMETHING WITH workbook HERE */
        };
        if(rABS) {
            reader.readAsBinaryString(file);
        } else {
            reader.readAsArrayBuffer(f);
        }
    }
    
    function trim(str) {
        while(str.indexOf(" ") >= 0) {
            str = str.replace(" ", "");
        }
        return str;
    }

    function callAPI(path, method, body) {
        let baseUrl = `${location.protocol}//${location.host}/api`;
        let url = baseUrl + path;
        let reqInint = {
            method: method
        };

        
        reqInint.headers = new Headers({
            'Content-Type': 'application/json'
        });
    
        reqInint.body = JSON.stringify(body);

        var responseChecking = function (res) {
            if (!res.ok && res.status < 500) {
                return Promise.reject(new Error(res.status + ' ' + res.statusText));
            }
    
            if (!res.ok && res.status >= 500) {
                return res.json().then(function (resJson) {
                    return Promise.reject(resJson);
                });
            }
    
            return res.json().then(function (resJson) {
                if (1 !== resJson.status) {
                    return Promise.reject(resJson);
                }
                return resJson.data;
            });
        };

        return window.fetch(url, reqInint).then((res) => {
            return responseChecking(res);
        });
    }
    
}());