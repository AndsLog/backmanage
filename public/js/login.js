(function () {
    
    $(document).on('click', '#submit', oauth);

    let regionTable = {
        "us-south": "ng",
        "eu-de" : "eu-de",
        "eu-gb" : "eu-gb",
        "au-syd" : "au-syd"
    };

    function oauth() {
        let name = $('#name').val();
        let password = $('#password').val();
        let regionUrl = $('#region').val();
        let body = {
            name: name,
            password: password,
            regionCode: regionTable[regionUrl]
        };
        $('#loading').removeClass('CSI-d-none');
        callAPI('/backstage/oauth','POST', body).then((resJson) => {
            let ibmToken = {
                "access_token": resJson.access_token,
                "token_type": resJson.token_type,
                "expires_in": resJson.expires_in,
                "regionCode": regionTable[regionUrl],
                "regionUrl": regionUrl,
                "username": name
            };
            localStorage.setItem("ibmToken", JSON.stringify(ibmToken));
            console.log(ibmToken);
            return;
        }).then(() => {
            window.location = '/home';
        }).catch((error) => {
            $('#loading').addClass('CSI-d-none');
            alert('登入失敗:' + error);
            console.log(error);
        });
    }

    function callAPI(path, method, body) {
        let baseUrl = `${location.protocol}//${location.host}/api`;
        let url = baseUrl + path;
        let headers = new Headers({
            'Content-Type': 'application/json'
        });
        let reqInint = {
            method: method,
            headers: headers
        };
        if(method === 'POST' && !(body instanceof String || typeof(body) === 'string')) {
            reqInint.body = JSON.stringify(body);
        }

        var responseChecking = function (res) {
            if (!res.ok && res.status < 500) {
                return Promise.reject(new Error(res.status + ' ' + res.statusText));
            }
    
            if (!res.ok && res.status >= 500) {
                return res.json().then(function (resJson) {
                    return Promise.reject(resJson);
                });
            }
    
            return res.json().then(function (resJson) {
                if (1 !== resJson.status) {
                    return Promise.reject(resJson);
                }
                return resJson.data;
            });
        };
        
        return window.fetch(url, reqInint).then((res) => {
            return responseChecking(res);
        });
    }
    
}());