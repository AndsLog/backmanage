(function() {
    const express = require('express');
    const app = express();
    const bodyParser = require('body-parser');
    const cloudantDBMdl = require('../models/cloudantdb');
    const ibmCFAPI = require('../services/ibmCloudFoundaryAPI');
    const ibmWAAPI = require('../services/ibmWatsonAssistantAPI');

    var baseUrl = '';
    // parse application/x-www-form-urlencoded
    app.use(bodyParser.urlencoded({
        extended: false
    }))

    // parse application/json
    app.use(bodyParser.json());

    function certificateController() {}

    certificateController.prototype.oauth = function(req, res) {
        let name = req.body.name;
        let password = req.body.password;
        let regionCode = req.body.regionCode;
        let ibmToken = {};

        return ibmCFAPI.oauth(name, password, regionCode).then((result) => {
            if(!result) {
                return Promise.reject(new Error('fail to SSO'));
            }
            ibmToken = Object.assign(ibmToken, result);
            ibmToken.regionCode = regionCode;
            return;
        }).then(() => {
            let resJson = {
                status: 1,
                data: ibmToken,
                msg: 'success to oauth'
            };
            res.status(200).json(resJson);
        }).catch((error) => {
            let resJson = {
                status: 0,
                msg: error
            };
            res.status(500).json(resJson);
        });
    }

    certificateController.prototype.getOrganizations = function(req, res) {
        let access_token = req.body.access_token;
        let token_type = req.body.token_type;
        return ibmCFAPI.getOrganizations(access_token, token_type).then((result) => {
            if(!result) {
                return Promise.reject(new Error('fail to get organizations'));
            }
            return result;
        }).then((data) => {
            let resJson = {
                status: 1,
                data: data,
                msg: 'success to get organizations'
            };
            res.status(200).json(resJson);
        }).catch((error) => {
            let resJson = {
                status: 0,
                msg: error
            };
            res.status(500).json(resJson);
        });
    }

    certificateController.prototype.getSpaces = function(req, res) {
        let access_token = req.body.access_token;
        let token_type = req.body.token_type;
        let orgGuid = req.params.orgguid;
        return ibmCFAPI.getSpaces(access_token, token_type, orgGuid).then((result) => {
            if(!result) {
                return Promise.reject(new Error('fail to get spaces'));
            }
            return result;
        }).then((data) => {
            let resJson = {
                status: 1,
                data: data,
                msg: 'success to get spaces'
            };
            res.status(200).json(resJson);
        }).catch((error) => {
            let resJson = {
                status: 0,
                msg: error
            };
            res.status(500).json(resJson);
        });
    }

    certificateController.prototype.getServices = function(req, res) {
        let access_token = req.body.access_token;
        let token_type = req.body.token_type;
        let spaceGuid = req.params.spaceguid;
        let totalResources;
        return ibmCFAPI.getServices(access_token, token_type, spaceGuid).then((result) => {
            if(!result) {
                return Promise.reject(new Error('fail to get spaces'));
            }
            return result;
        }).then((result) => {
            let nextUrl = result.next_url;
            let resources = result.resources;
            if (nextUrl) {
                totalResources = resources;
                return ibmCFAPI.getServices(access_token, token_type, nextUrl, true).then((result) => {
                    if(!result) {
                        return Promise.reject(new Error('fail to get spaces'));
                    }
                    return result;
                }).then((result) =>{
                    let resources = result.resources;
                    totalResources = totalResources.concat(resources);
                    result.resources = totalResources;
                    return result;
                });
            }
            return result;
        }).then((data) => {
            let resJson = {
                status: 1,
                data: data,
                msg: 'success to get spaces'
            };
            res.status(200).json(resJson);
        }).catch((error) => {
            let resJson = {
                status: 0,
                msg: error
            };
            res.status(500).json(resJson);
        });
    }

    certificateController.prototype.getWatsonList = function(req, res) {
        let access_token = req.body.access_token;
        let token_type = req.body.token_type;
        let serviceGuid = req.params.serviceguid;
        let tableName = 'after_watson';

        return ibmCFAPI.getServicekeys(access_token, token_type, serviceGuid).then((result) => {
            let DBusername = result.resources[0].entity.credentials.username;
            let DBpassword = result.resources[0].entity.credentials.password;
            return cloudantDBMdl.initDB(DBusername, DBpassword);
        }).then((result) => {
            if (!result) {
                return Promise.reject(new Error('DB init fail'));
            }
            req.DB = result;
            let viewName = 'getWatson';
            return cloudantDBMdl.view(req.DB, tableName, viewName);
        }).then((data) => {
            let resJson = {
                status: 1,
                data: data,
                msg: 'success to get watson list'
            };
            res.status(200).json(resJson);
        }).catch((error) => {
            let resJson = {
                status: 0,
                msg: error
            };
            res.status(500).json(resJson);
        })
    }

    certificateController.prototype.getWorkspace = function(req, res) {
        let access_token = req.body.access_token;
        let token_type = req.body.token_type;
        let watsonName = req.params.name;

        let watsonGuid;

        return ibmCFAPI.getServiceInstance(access_token, token_type, watsonName).then((result) => {
            watsonGuid = result.resources[0].metadata.guid;
            return ibmCFAPI.getServicekeys(access_token, token_type, watsonGuid);
        }).then((result) => {
            let WAusername = result.resources[0].entity.credentials.username;
            let WApassword = result.resources[0].entity.credentials.password;
            return ibmWAAPI.initWatson(WAusername, WApassword);
        }).then((result) => {
            if (!result) {
                return Promise.reject(new Error('watson init fail'));
            }
            req.watson = result;
            return ibmWAAPI.listWorkspace(req.watson);
        }).then((result) => {
            result.serviceGuid = watsonGuid
            return result;
        }).then((data) => {
            let resJson = {
                status: 1,
                data: data,
                msg: 'success to get organizations'
            };
            res.status(200).json(resJson);
        }).catch((error) => {
            let resJson = {
                status: 0,
                msg: error
            };
            res.status(500).json(resJson);
        })
    }

    module.exports = new certificateController();
}());