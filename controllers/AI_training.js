(function () {
    const cloudantDBMdl = require('../models/cloudantdb');
    const ibmWAAPI = require('../services/ibmWatsonAssistantAPI');
    const ibmCFAPI = require('../services/ibmCloudFoundaryAPI');
    function AI_trainigController() {};

    AI_trainigController.prototype.getWatsonSay = function(req, res) {
        let access_token = req.body.access_token;
        let token_type = req.body.token_type;
        let startTime = req.body.startTime || '';
        let endTime = req.body.endTime || '';
        let watsonName = req.params.name;
        let serviceGuid = req.params.serviceguid;
        let watsonSay;
        let workspaceIntentList = {};
        let tableName = 'after_watson';

        let workspaceId;

        return ibmCFAPI.getServicekeys(access_token, token_type, serviceGuid).then((result) => {
            let DBusername = result.resources[0].entity.credentials.username;
            let DBpassword = result.resources[0].entity.credentials.password;
            return cloudantDBMdl.initDB(DBusername, DBpassword);
        }).then((result) => {
            if (!result) {
                return Promise.reject(new Error('DB init fail'));
            }
            req.DB = result;
            return ibmCFAPI.getServiceInstance(access_token, token_type, watsonName).then((result) => {
                watsonGuid = result.resources[0].metadata.guid;
                return ibmCFAPI.getServicekeys(access_token, token_type, watsonGuid);
            });
        }).then((result) => {
            let WAusername = result.resources[0].entity.credentials.username;
            let WApassword = result.resources[0].entity.credentials.password;
            return ibmWAAPI.initWatson(WAusername, WApassword);
        }).then((result) => {
            if (!result) {
                return Promise.reject(new Error('watson init fail'));
            }
            req.watson = result;
            return cloudantDBMdl.get(req.DB, tableName);
        }).then((result) => {
            if(!result) {
                return cloudantDBMdl.createDB(req.DB, logTableName).then((result) => {
                    if(!result) {
                        return Promise.reject(new Error('DB create fail'));
                    }
                    let indexContent = {
                        name: 'timestamp',
                        type: 'json',
                        index: {
                            fields:[
                                {'timestamp': 'desc'}
                            ]
                        }
                    }
                    return cloudantDBMdl.DBindex(req.DB, logTableName, indexContent).then((result) => {
                        if(!result) {
                            return Promise.reject(new Error('DB index create fail'));
                        }
                        return;
                    });
                })
            }
            return;
        }).then(() => {
            let query = {
                selector: {
                    "watsonName": watsonName
                },
                sort: [{"timestamp": "desc"}]
            };
            if(startTime) {
                let startTimeQuery = {
                    "$gte" : startTime
                };
                query.selector.timestamp = Object.assign({}, startTimeQuery);
            }
            if (endTime) {
                if (!query.selector.timestamp) {
                    query.selector.timestamp = {};
                }
                let endTimeQuery = {
                    "$lte" : endTime
                };
                query.selector.timestamp = Object.assign(query.selector.timestamp, endTimeQuery);
            }
            return cloudantDBMdl.find(req.DB, tableName, query);
        }).then((queryResult) =>{
            if (!queryResult) {
                return Promise.reject(new Error('fail to find'));
            }
            watsonSay = queryResult;
            let viewName = 'getWatson';
            return cloudantDBMdl.view(req.DB, tableName, viewName);
        }).then((viewData) => {
            let rows = viewData.rows;
            return Promise.all(rows.map((row) => {
                if(row.key.watsonName === watsonName) {
                    workspaceId = row.key.workspaceId;
                    return ibmWAAPI.listIntent(req.watson, workspaceId).then((intents) => {
                        if (!intents) {
                            return Promise.resolve();
                        }
                        workspaceIntentList[workspaceId] = {
                            intents: intents
                        };
                        return Promise.resolve();
                    });
                }
                return Promise.resolve();
            }));
        }).then(() => {
            watsonSay.workspaceIntentList = workspaceIntentList;
            watsonSay.watsonWorkSpaceId = workspaceId;
            return watsonSay;
        }).then((watsonSay) => {
            let resJson = {
                status: 1,
                data: watsonSay,
                msg: 'success to get watson'
            };
            res.status(200).json(resJson);
        }).catch((error) => {
            let resJson = {
                status: 0,
                msg: error
            };
            res.status(500).json(resJson);
        });
    }

    AI_trainigController.prototype.getWatsonTrainingLog = function(req,res) {
        let access_token = req.body.access_token;
        let token_type = req.body.token_type;
        let startTime = req.body.startTime || '';
        let endTime = req.body.endTime || '';
        let watsonName = req.params.name;
        let serviceGuid = req.params.serviceguid;
        let tableName = 'watson_training_log';

        return ibmCFAPI.getServicekeys(access_token, token_type, serviceGuid).then((result) => {
            let DBusername = result.resources[0].entity.credentials.username;
            let DBpassword = result.resources[0].entity.credentials.password;
            return cloudantDBMdl.initDB(DBusername, DBpassword);
        }).then((result) => {
            if (!result) {
                return Promise.reject(new Error('DB init fail'));
            }
            req.DB = result;
            return;
        }).then(() => {
            let query = {
                selector: {
                    "watsonName": watsonName
                },
                sort: [{"timestamp": "desc"}]
            };
            if(startTime) {
                let startTimeQuery = {
                    "$gte" : startTime
                };
                query.selector.timestamp = Object.assign({}, startTimeQuery);
            }
            if (endTime) {
                if (!query.selector.timestamp) {
                    query.selector.timestamp = {};
                }
                let endTimeQuery = {
                    "$lte" : endTime
                };
                query.selector.timestamp = Object.assign(query.selector.timestamp, endTimeQuery);
            }
            return cloudantDBMdl.find(req.DB, tableName, query);
        }).then((data) => {
            let resJson = {
                status: 1,
                data: data,
                msg: 'success to get training log'
            };
            res.status(200).json(resJson);
        }).catch((error) => {
            let resJson = {
                status: 0,
                msg: error
            };
            res.status(500).json(resJson);
        })
    }

    AI_trainigController.prototype.pushExample = function(req, res) {
        let access_token = req.body.access_token;
        let token_type = req.body.token_type;
        let docId = req.body.docId;
        let updateIntent = req.body.updateIntent;
        let intent = req.body.intent;
        let username = req.body.username;
        
        let watsonName = req.params.name;
        let serviceGuid = req.params.serviceguid;
        let workspaceId = req.params.workspaceid;
        let text = req.body.text;
        let tableName = 'after_watson';
        let logTableName = 'watson_training_log'
        let findedDoc;

        return ibmCFAPI.getServiceInstance(access_token, token_type, watsonName).then((result) => {
            watsonGuid = result.resources[0].metadata.guid;
            return ibmCFAPI.getServicekeys(access_token, token_type, watsonGuid);
        }).then((result) => {
            let WAusername = result.resources[0].entity.credentials.username;
            let WApassword = result.resources[0].entity.credentials.password;
            return ibmWAAPI.initWatson(WAusername, WApassword);
        }).then((result) => {
            if(!result) {
                return Promise.reject(new Error('watson init fail'));
            }
            req.watson = result;
            return ibmCFAPI.getServicekeys(access_token, token_type, serviceGuid);
        }).then((result) => {
            let DBusername = result.resources[0].entity.credentials.username;
            let DBpassword = result.resources[0].entity.credentials.password;
            return cloudantDBMdl.initDB(DBusername, DBpassword);
        }).then((result) => {
            if (!result) {
                return Promise.reject(new Error('DB init fail'));
            }
            req.DB = result;
            let query = {
                "selector": {
                    "_id": {
                        "$eq": docId
                    }
                },
                sort: [{"timestamp": "desc"}]
            };
            return cloudantDBMdl.find(req.DB, tableName, query);
        }).then((queryResult) => {
            if(!queryResult) {
                return Promise.reject(new Error('data find fail'));
            }

            findedDoc = queryResult;

            return ibmWAAPI.getExample(req.watson, workspaceId, intent, text); 
        }).then((result) => {
            if(result){
                return ibmWAAPI.deleteExample(req.watson, workspaceId, intent, text); 
            }
            return;
        }).then(() => {
            return ibmWAAPI.createExample(req.watson, workspaceId, updateIntent, text);
        }).then((result) => {
            if(!result) {
                return Promise.reject(new Error('example create fail '))
            }

            findedDoc.docs[0].updateIntent = updateIntent;
            let putData = findedDoc.docs[0];
            return cloudantDBMdl.updateIntent(req.DB, tableName, putData);
        }).then((result) => {
            if(!result.ok) {
                return Promise.reject(new Error('example update fail '))
            }

            return cloudantDBMdl.get(req.DB, logTableName)
        }).then((result) => {
            if(!result) {
                return cloudantDBMdl.createDB(req.DB, logTableName).then((result) => {
                    if(!result) {
                        return Promise.reject(new Error('DB create fail'));
                    }
                    let indexContent = {
                        name: 'timestamp',
                        type: 'json',
                        index: {
                            fields:[
                                {'timestamp': 'desc'}
                            ]
                        }
                    }
                    return cloudantDBMdl.DBindex(req.DB, logTableName, indexContent).then((result) => {
                        if(!result) {
                            return Promise.reject(new Error('DB index create fail'));
                        }
                        return;
                    });
                })
            }
            return;
        }).then(() => {
            let pushLog = {
                watsonName: watsonName,
                workspaceId: workspaceId,
                example: findedDoc.docs[0].input.text,
                intent: findedDoc.docs[0].intents.length === 0 ? '無' : findedDoc.docs[0].intents[0].intent,
                updateIntent: findedDoc.docs[0].updateIntent,
                username: username,
                timestamp: Date.now()
            }

            return cloudantDBMdl.insert(req.DB, logTableName, pushLog);
        }).then((result) => {
            if(!result.ok) {
                return Promise.reject(new Error('fail to insert'));
            }
            return findedDoc;
        }).then((data) => {
            let resJson = {
                status: 1,
                data: data,
                msg: 'success to create example'
            };
            res.status(200).json(resJson);
        }).catch((error) => {
            let resJson = {
                status: 0,
                msg: error
            };
            res.status(500).json(resJson);
        })
    }

    module.exports = new AI_trainigController();
}());