(function () {
    const cloudantDBMdl = require('../models/cloudantdb');
    const ibmCFAPI = require('../services/ibmCloudFoundaryAPI');
    function usageController() {};

    usageController.prototype.getDailyUsage = function(req, res) {
        let access_token = req.body.access_token;
        let token_type = req.body.token_type;
        let userName = req.body.username;
        let type = req.body.type;

        let orgGuid = req.params.orgguid;
        let spaceGuid = req.params.spaceguid;

        let firstDay = req.body.firstDay;
        let lastDay = req.body.lastDay;

        let tableName;
        if (type === 'daily') {
            tableName = 'daily_usage';
        }
        if (type === 'monthly') {
            tableName = 'monthly_usage';
        }
        let instanceName = 'serviceBotUsage-cloudantNoSQLDB';

        let userNameOrgId;
        let userNamespaceId;

        return ibmCFAPI.getOrganizations(access_token, token_type).then((result) => {
            let organizations = result.resources;
            if(!organizations.length === 0) {
                return Promise.reject(new Error('fail to get organizations'));
            }
            organizations.map((organization) => {
                let emailRule = /^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z]+$/;
                // if(userName === organization.entity.name || organization.entity.name.search(emailRule)!== -1) {
                //     userNameOrgId = organization.metadata.guid;
                // }
                if(userName === organization.entity.name) {
                    userNameOrgId = organization.metadata.guid;
                }
            });
            return ibmCFAPI.getSpaces(access_token, token_type, userNameOrgId);
        }).then((result) => {
            let spaces = result.resources;
            if(!spaces.length === 0) {
                return Promise.reject(new Error('fail to get spaces'));
            }
            spaces.map((space) => {
                if('dev' === space.entity.name) {
                    userNamespaceId = space.metadata.guid;
                }
            })
            return ibmCFAPI.getServiceInstance(access_token, token_type, instanceName, userNamespaceId);
        }).then((result) => {
            let serviceGuid = result.resources[0].metadata.guid;
            return ibmCFAPI.getServicekeys(access_token, token_type, serviceGuid)
        }).then((result) => {
            let DBusername = result.resources[0].entity.credentials.username;
            let DBpassword = result.resources[0].entity.credentials.password;
            return cloudantDBMdl.initDB(DBusername, DBpassword);
        }).then((result) => {
            if (!result) {
                return Promise.reject(new Error('DB init fail'));
            }
            req.DB = result;
            let query = {
                selector: {
                    "organizationId": orgGuid,
                    "spaceId": spaceGuid,
                    "timestamp": {
                        "$gte": firstDay,
                        "$lte": lastDay
                    }
                },
                sort: [{"timestamp": "asc"}]
            }
            return cloudantDBMdl.find(req.DB, tableName, query);
        }).then((queryResult) => {
            if (!queryResult) {
                return Promise.reject(new Error('fail to find'));
            }
            return queryResult;
        }).then((data) => {
            let resJson = {
                status: 1,
                data: data,
                msg: 'success to get daily usage'
            };
            res.status(200).json(resJson);
        }).catch((error) => {
            let resJson = {
                status: 0,
                msg: error
            };
            res.status(500).json(resJson);
        });
    }

    usageController.prototype.fetchDailyUsage = function(req, res) {
        let access_token = req.body.access_token;
        let token_type = req.body.token_type;
        let regionCode = req.body.regionCode;
        let regionUrl = req.body.regionUrl;
        let userName = req.body.username;

        let dailyTableName = 'daily_usage';
        let monthlyTableName = 'monthly_usage';
        let instanceName = 'serviceBotUsage-cloudantNoSQLDB';

        let userNameOrgId;
        let userNamespaceId;
        let orgsAndspacesMap = {};

        // 本日之前一天的unix time
        let unixNowDate = new Date().setHours(0, 0, 0, 0);
        let unixPastOneDate = unixNowDate - 86400000;

        // 本月之前一個月的unix time
        let nowYear = new Date().getFullYear();
        let nowMonth = new Date().getMonth();
        let unixPastMonth = new Date(nowYear + '-' + nowMonth);

        return ibmCFAPI.getOrganizations(access_token, token_type).then((result) => {
            let organizations = result.resources;
            if(!organizations.length === 0) {
                return Promise.reject(new Error('fail to get organizations'));
            }
            organizations.map((organization) => {
                let emailRule = /^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z]+$/;
                let orgId = organization.metadata.guid;
                let createTime = new Date(organization.metadata.created_at).setHours(0, 0, 0, 0);
                orgsAndspacesMap[orgId] = {
                    createTime: createTime,
                    name: organization.entity.name
                }
                // if(userName === organization.entity.name || organization.entity.name.search(emailRule)!== -1) {
                //     userNameOrgId = organization.metadata.guid;
                // }
                if(userName === organization.entity.name) {
                    userNameOrgId = organization.metadata.guid;
                }
            });
            return ibmCFAPI.getSpaces(access_token, token_type, userNameOrgId);
        }).then((result) => {
            let spaces = result.resources;
            if(!spaces) {
                return Promise.reject(new Error('fail to get spaces'));
            }
            spaces.map((space) => {
                if('dev' === space.entity.name) {
                    userNamespaceId = space.metadata.guid;
                }
            })
            return ibmCFAPI.getServiceInstance(access_token, token_type, instanceName, userNamespaceId);
        }).then((result) => {
            if(result.resources.length === 0) {
                return ibmCFAPI.getCloudantDBLite(access_token, token_type).then((result) => {
                    if(!result) {
                        return Promise.reject(new Error('fail to get cloudantDBLite'));
                    }
                    let servicePlanGuid = result.resources[0].metadata.guid;
                    return ibmCFAPI.createCloudantDB(access_token, token_type, instanceName, servicePlanGuid, userNamespaceId).then((result) => {
                        if(!result) {
                            return Promise.reject(new Error('fail to create cloudantDBLite'));
                        }
                        let certification = {
                            resources: []
                        };
                        certification.resources.push(result);
                        return certification;
                    })
                })
            }
            return result;
        }).then((result) => {
            let serviceGuid = result.resources[0].metadata.guid;
            return ibmCFAPI.getServicekeys(access_token, token_type, serviceGuid).then((result) => {
                if(result.resources.length === 0) {
                    let keyName = 'DB-key';
                    return ibmCFAPI.createServiceKey(access_token, token_type, keyName, serviceGuid).then((credentials) => {
                        let resources = [];
                        let result = {};
                        resources.push(credentials);
                        result.resources = resources;
                        return result;
                    })
                }
                return result;
            })
        }).then((result) => {
            let DBusername = result.resources[0].entity.credentials.username;
            let DBpassword = result.resources[0].entity.credentials.password;
            return cloudantDBMdl.initDB(DBusername, DBpassword);
        }).then((result) => {
            if (!result) {
                return Promise.reject(new Error('DB init fail'));
            }
            req.DB = result;
            return cloudantDBMdl.get(req.DB, dailyTableName);
        }).then((result) => {
            if(!result) {
                return cloudantDBMdl.createDB(req.DB, dailyTableName).then((result) => {
                    if(!result) {
                        return Promise.reject(new Error('DB create fail'));
                    }
                    return;
                }).then(() => {
                    let indexContent = {
                        name: 'timestamp',
                        type: 'json',
                        index: {
                            fields:[
                                {'timestamp': 'desc'}
                            ]
                        }
                    }
                    return cloudantDBMdl.DBindex(req.DB, dailyTableName, indexContent);
                }).then((result) => {
                    if(!result) {
                        return Promise.reject(new Error('DB index create fail'));
                    }
                    return;
                })
            }
            return;
        }).then(() => {
            let orgIds = Object.keys(orgsAndspacesMap);
            return Promise.all(orgIds.map((orgId) => {
                let query = {
                    selector: {
                        "timestamp": {
                            "$lte": unixPastOneDate
                        },
                        "organizationId": orgId
                    },
                    sort: [{"timestamp": "desc"}]
                }
                return cloudantDBMdl.find(req.DB, dailyTableName, query).then((usages) => {
                    let usageLength = Object.keys(usages).length;
                    if (usageLength > 0) {
                        orgsAndspacesMap[orgId].dailyLatestTime = usages.docs[0].timestamp + 86400000;
                    }
                    return Promise.resolve();
                });
            }));
        }).then(() => {
            let orgIds = Object.keys(orgsAndspacesMap);

            let resJson = {
                status: 1,
                msg: 'beginning to get usage and insert to DB'
            };
            res.status(200).json(resJson);

            return Promise.all(orgIds.map((orgId) => {
                let name = orgsAndspacesMap[orgId].name;
                let createTime = new Date(orgsAndspacesMap[orgId].createTime).setHours(0, 0, 0, 0);
                let latestTime = new Date(orgsAndspacesMap[orgId].dailyLatestTime).setHours(0, 0, 0, 0) || '';

                // 若帳號創辦日期比2018年1月1日還早，以2018年1月1日開始取，每天的量
                let timeBoundary = new Date('2018-1-1').setHours(0, 0, 0, 0);
                let nearestTime;

                nearestTime = latestTime;
                if(!latestTime && createTime > timeBoundary) {
                    nearestTime = createTime;
                }

                if(!latestTime && createTime < timeBoundary) {
                    nearestTime = timeBoundary;
                }

                if(nearestTime < unixPastOneDate) {

                    console.log(`${name} beginning to get usage and insert to DB`);

                    let i = 0;
                    return usageController.prototype._fetchDaily(req.DB, i, dailyTableName, access_token, token_type, regionCode, regionUrl, orgId, name, nearestTime, unixPastOneDate).then((result) => {
                        if(!result) {
                            console.log(`_fetchDaily : ${result}`);
                            return Promise.resolve();
                        }
                        console.log(`success to insert`);
                        return Promise.resolve();
                    });
                } else {
                    console.log(`${name} usage is latest`);
                    return Promise.resolve();
                }
            }));
        }).then(() => {
            return cloudantDBMdl.get(req.DB, monthlyTableName);
        }).then((result) => {
            if(!result) {
                return cloudantDBMdl.createDB(req.DB, monthlyTableName).then((result) => {
                    if(!result) {
                        return Promise.reject(new Error('DB create fail'));
                    }
                    return;
                }).then(() => {
                    let indexContent = {
                        name: 'timestamp',
                        type: 'json',
                        index: {
                            fields:[
                                {'timestamp': 'desc'}
                            ]
                        }
                    }
                    return cloudantDBMdl.DBindex(req.DB, monthlyTableName, indexContent);
                }).then((result) => {
                    if(!result) {
                        return Promise.reject(new Error('DB index create fail'));
                    }
                    return;
                })
            }
            return;
        }).then(() => {
            let orgIds = Object.keys(orgsAndspacesMap);
            return Promise.all(orgIds.map((orgId) => {
                let query = {
                    selector: {
                        "timestamp": {
                            "$lte": unixPastOneDate
                        },
                        "organizationId": orgId
                    },
                    sort: [{"timestamp": "desc"}]
                }
                return cloudantDBMdl.find(req.DB, monthlyTableName, query).then((usages) => {
                    let usageLength = Object.keys(usages).length;
                    if (usageLength > 0) {
                        // 知道目前資料庫最新資料的時間
                        // 將時間移到下一個月
                        let year = new Date(usages.docs[0].timestamp).getFullYear();
                        let month = new Date(usages.docs[0].timestamp).getMonth() + 1;
                        let nextMonth = month + 1;
                        if (nextMonth > 12) {
                            nextMonth = 0;
                            year = year + 1;
                        }
                        let DBYearMonth = new Date(`${year}-${nextMonth}`).getTime();
                        orgsAndspacesMap[orgId].monthlyLatestTime = DBYearMonth;
                    }
                    return Promise.resolve();
                });
            }));
        }).then(() => {
            let orgIds = Object.keys(orgsAndspacesMap);
            return Promise.all(orgIds.map((orgId) => {
                let name = orgsAndspacesMap[orgId].name;

                // 帳號創立的下一個月才會有結算資料
                let createTimeYear = new Date(orgsAndspacesMap[orgId].createTime).getFullYear();
                let createTimeNextMonth = new Date(orgsAndspacesMap[orgId].createTime).getMonth() + 2;
                if (createTimeNextMonth >= 12) {
                    createTimeYear = createTimeYear + 1;
                    createTimeNextMonth = 0;
                }
                let createTimeYearNextMonth = new Date(`${createTimeYear}-${createTimeNextMonth}`).getTime();
                
                let latestTimeYear = new Date(orgsAndspacesMap[orgId].monthlyLatestTime).getFullYear() || '';
                let latestTimeMonth = new Date(orgsAndspacesMap[orgId].monthlyLatestTime).getMonth() + 1 || '';
                let latestTimeYearMonth = new Date(`${latestTimeYear}-${latestTimeMonth}`).getTime();

                // 若帳號創辦日期比2018年1月，以2018年1月開始取
                let timeBoundary = new Date('2018-1').getTime();
                let nearestMonth;

                nearestMonth = latestTimeYearMonth;

                if(!latestTimeYearMonth && createTimeYearNextMonth > timeBoundary) {
                    nearestMonth = createTimeYearNextMonth;
                }

                
                if(!latestTimeYearMonth && createTimeYearNextMonth < timeBoundary) {
                    nearestMonth = timeBoundary;
                }

                if(nearestMonth <= unixPastMonth) {

                    console.log(`${name} beginning to get usage and insert to DB`);

                    let i = 0;
                    return usageController.prototype._fetchMonthly(req.DB, i, monthlyTableName, access_token, token_type, regionCode, regionUrl, orgId, name, nearestMonth, unixPastMonth).then((result) => {
                        if(!result) {
                            console.log(`_fetchMonthly : ${result}`);
                            return Promise.resolve();
                        }
                        console.log(`success to insert`);
                        return Promise.resolve();
                    });
                } else {
                    console.log(`${name} usage is latest`);
                    return Promise.resolve();
                }
            }));
        }).then((results) => {
            console.log(results);
        }).catch((error) => {
            console.log(error);
            return;
        });
    }

    usageController.prototype._fetchDaily = function(DB, i, tableName, access_token, token_type, regionCode, regionUrl, orgGuid, name, time, endTime) {
        if(time >= endTime) {
            console.log(`${name} success to insert daily usage to DB`);
            return true;
        }

        let firstDay = new Promise((resolve, reject) => {
            let unixFirstTimeZeroHour = new Date(time).setHours(0, 0, 0, 0);
            let year = new Date(unixFirstTimeZeroHour).getFullYear();
            let month = new Date(unixFirstTimeZeroHour).getMonth() + 1;
            let date = new Date(unixFirstTimeZeroHour).getDate();
            let dateTime = `${year}-${month}-${date}`;
            console.log(`${name} firstDay: ${dateTime}`);
            return ibmCFAPI.getUsage(access_token, token_type, regionCode, regionUrl, orgGuid, dateTime).then((result) => {
                if(!result) {
                    return resolve(null);
                }
                result.dateTime = dateTime;
                result.timestamp = unixFirstTimeZeroHour;
                result.organizationId = orgGuid;
                return resolve(result);
            });
        });

        time = time + 86400000;

        let nextDay = new Promise((resolve, reject) => {
            let unixNextTimeZeroHour = new Date(time).setHours(0, 0, 0, 0);
            let year = new Date(unixNextTimeZeroHour).getFullYear();
            let month = new Date(unixNextTimeZeroHour).getMonth() + 1;
            let date = new Date(unixNextTimeZeroHour).getDate();
            let dateTime = `${year}-${month}-${date}`;
            console.log(`${name} nextDay: ${dateTime}`);
            return ibmCFAPI.getUsage(access_token, token_type, regionCode, regionUrl, orgGuid, dateTime).then((result) => {
                if(!result) {
                    return resolve(null);
                }
                result.dateTime = dateTime;
                result.timestamp = unixNextTimeZeroHour;
                result.organizationId = orgGuid;
                return resolve(result);
            });
        });
        return Promise.all([firstDay, nextDay]).then((dts) => {
            let firstDay = dts[0];
            let nextDay = dts[1];
            if(!firstDay || !nextDay) {
                return null;
            }
            let firstDaySpaces = usageController.prototype._flatUsage(firstDay);
            let nextDaySpaces = usageController.prototype._flatUsage(nextDay);
            if (!firstDaySpaces || !nextDaySpaces) {
                return null;
            }
            let daySpaces = usageController.prototype._countDailyCost(i, firstDaySpaces, nextDaySpaces);
            return daySpaces;
        }).then((daySpaces) => {
            if(!daySpaces) {
                return null;
            }
            let daySpaceIds = Object.keys(daySpaces);
            return Promise.all(daySpaceIds.map((daySpaceId) => {
                let spaceProducts = daySpaces[daySpaceId];
                spaceProducts.spaceId = daySpaceId;
                return cloudantDBMdl.insert(DB, tableName, spaceProducts).then((result) => {
                    if(!result) {
                        console.log(`${spaceProducts.dateTime} ${name} space: ${spaceProducts.name} fail to insert`);
                        return Promise.reject();
                    }
                    console.log(`${spaceProducts.dateTime} ${name} space: ${spaceProducts.name} success to insert`);
                    return Promise.resolve();
                })
            }));
        }).then((result) => {
            if(!result) {
                return null;
            }
            
            i++;
            usageController.prototype._fetchDaily(DB, i, tableName, access_token, token_type, regionCode, regionUrl, orgGuid, name, time, endTime);
        }).catch((error) => {
            console.log(error);
        })
    }

    usageController.prototype._fetchMonthly = function(DB, i, tableName, access_token, token_type, regionCode, regionUrl, orgGuid, name, time, endTime) {
        if(time > endTime) {
            console.log(`${name} success to insert daily usage to DB`);
            return true;
        }

        let unixFirstTimeZeroHour = new Date(time).setHours(0, 0, 0, 0);
        let year = new Date(unixFirstTimeZeroHour).getFullYear();
        let month = new Date(unixFirstTimeZeroHour).getMonth() + 1;
        let monthTime = `${year}-${month}`;
        console.log(`${name} month: ${monthTime}`);
        return ibmCFAPI.getUsage(access_token, token_type, regionCode, regionUrl, orgGuid, monthTime).then((result) => {
            if(!result) {
                return null;
            }
            result.dateTime = monthTime;
            result.timestamp = unixFirstTimeZeroHour;
            result.organizationId = orgGuid;
            return result;
        }).then((monthUsage) => {
            if(!monthUsage) {
                return null;
            }

            let monthUsageSpaces = usageController.prototype._flatUsage(monthUsage);
            if (!monthUsageSpaces) {
                return null;
            }
            let monthSpaceIds = Object.keys(monthUsageSpaces);
            return Promise.all(monthSpaceIds.map((monthSpaceId) => {
                let spaceProducts = monthUsageSpaces[monthSpaceId];
                spaceProducts.spaceId = monthSpaceId;
                return cloudantDBMdl.insert(DB, tableName, spaceProducts).then((result) => {
                    if(!result) {
                        console.log(`${spaceProducts.dateTime} ${name} space: ${spaceProducts.name} fail to insert`);
                        return Promise.reject();
                    }
                    console.log(`${spaceProducts.dateTime} ${name} space: ${spaceProducts.name} success to insert`);
                    return Promise.resolve();
                })
            }));
        }).then((result) => {
            if(!result) {
                return null;
            }

            // 將時間移到下一個月
            let nextMonth = month + 1;

            // 十二月的下一個月為隔年的一月
            if (nextMonth > 12) {
                nextMonth = 0;
                year = year + 1;
            }
            time = new Date(`${year}-${nextMonth}`).getTime();
            
            i++;
            usageController.prototype._fetchMonthly(DB, i, tableName, access_token, token_type, regionCode, regionUrl, orgGuid, name, time, endTime);
        }).catch((error) => {
            console.log(error);
        })
    }

    usageController.prototype._flatUsage = function(result){
        if(result.organizations.length === 0 || result.organizations[0].billable_usage.spaces.length === 0) {
            return null;
        }
        let dateTime = result.dateTime;
        let timestamp = result.timestamp;
        let organizationId = result.organizationId;
        let organization = result.organizations[0];
        let organizationName = organization.name;
        let billable_spaces = organization.billable_usage.spaces;
        let spaceUsage = {};
        let spaceName;
        let serviceName;
        let applicationName;
        let instanceName;

        let spaceId;
        let applicationId;
        let serviceId;
        let instanceId;

        billable_spaces.map((space) => {
            spaceName = space.name; // 空間名稱
            spaceId = space.id;
            let applications = space.applications;
            let services = space.services;
            spaceUsage[spaceId] = {
                dateTime: dateTime,
                timestamp: timestamp,
                name: spaceName,
                organizationName: organizationName,
                organizationId: organizationId,
                products: {}
            };
            applications.map((application) => {
                applicationName = application.name; // 自取的應用程式名稱
                applicationId = application.id;
                let usages = application.usage;
                usages.map((usage) => {
                    let buildpack = usage.buildpack; // 應用程式名稱
                    let cost = usage.cost; // 應用程式花費
                    let quantity = usage.quantity;
                    let unit = usage.unit; // 計費方式
                    let unitId = usage.unitId; // 結算方式
                    let itemUsage = {
                        'cost': cost,
                        'quantity': quantity,
                        'unit': unit
                    };
                    if(!spaceUsage[spaceId].products[buildpack]) {
                        spaceUsage[spaceId].products[buildpack] = {
                            items:{}
                        };
                    }
                    if(!spaceUsage[spaceId].products[buildpack].items[applicationId]){
                        spaceUsage[spaceId].products[buildpack].items[applicationId] = {
                            name: '',
                            usage: {}
                        };
                    }
                    spaceUsage[spaceId].products[buildpack].items[applicationId].name = applicationName;
                    spaceUsage[spaceId].products[buildpack].items[applicationId].usage[unitId] = itemUsage;
                });
            });
            services.map((service) => {
                serviceName = service.name; //服務名稱
                serviceId = service.id;
                let instances = service.instances;
                instances.map((instance) => {
                    instanceName = instance.name; // 自取的服務名稱
                    instanceId = instance.id;
                    let usages = instance.usage;
                    usages.map((usage) => {
                        let applicationId = usage.applicationId;
                        let cost = usage.cost; // 服務花費
                        let quantity = usage.quantity;
                        let unit = usage.unit; // 計費方式
                        let unitId = usage.unitId; // 結算方式
                        let itemUsage = {
                            'cost': cost,
                            'quantity': quantity,
                            'unit': unit,
                        }
                        if(!spaceUsage[spaceId].products[serviceId]) {
                            spaceUsage[spaceId].products[serviceId] = {
                                name: serviceName,
                                items: {}
                            };
                        }
                        if(!spaceUsage[spaceId].products[serviceId].items[instanceId]) {
                            spaceUsage[spaceId].products[serviceId].items[instanceId] = {
                                name: '',
                                usage: {}
                            }
                        }
                        spaceUsage[spaceId].products[serviceId].items[instanceId].name = instanceName;
                        spaceUsage[spaceId].products[serviceId].items[instanceId].usage[unitId] = itemUsage;
                    })
                })
            });
        });
        return spaceUsage;
    }

    usageController.prototype._countDailyCost = function(i ,firstDaySpaces, nextDaySpaces) {
        let firstDaySpaceIds = Object.keys(firstDaySpaces);
        let firstDaySpaceNum = firstDaySpaceIds.length; // 第一天的space數量
        let nextDaySpaceIds = Object.keys(nextDaySpaces);
        let nextDaySpaceNum = nextDaySpaceIds.length; // 第二天的space數量
        let newSpace = {};
        let spaceIds;

        // 第一天比第二天多，表示有space被刪除
        // 處理第二天還有的space就好
        if(firstDaySpaceNum > nextDaySpaceNum) {
            spaceIds = nextDaySpaceIds.filter((nextDaySpaceId) => {
                return firstDaySpaceIds.indexOf(nextDaySpaceId) >= 0;
            });
        }

        // 狀況一：反之，表示有新增的space
        // 狀況二：space數量相同，有可能新增一個space再刪除另一個
        // 要將第二天多的space取出，待usage計算完後，加入firstDay中
        if(firstDaySpaceNum <= nextDaySpaceNum) {
            spaceIds = nextDaySpaceIds.filter((nextDaySpaceId) => {
                if(firstDaySpaceIds.indexOf(nextDaySpaceId) === -1) {
                    newSpace[nextDaySpaceId] = nextDaySpaces[nextDaySpaceId];
                }
                return firstDaySpaceIds.indexOf(nextDaySpaceId) >= 0;
            })
        }
        // console.log('prepare into space map', i);
        spaceIds.map((spaceId, i) => {
            let firstDayProductIds = Object.keys(firstDaySpaces[spaceId].products);
            let firstDayProductIdLength = firstDayProductIds.length; //第一天app產品數量
            
            let nextDayProductIds = Object.keys(nextDaySpaces[spaceId].products);
            let nextDayProductIdLength = nextDayProductIds.length; //第二天app產品數量

            let newProduct = {};
            let productIds;

            // 第一天產品數比第二天多，表示有被刪除的產品
            // 處理第二天的就好
            if(firstDayProductIdLength > nextDayProductIdLength) {
                productIds = nextDayProductIds.filter((nextDayProductId) => {
                    return firstDayProductIds.indexOf(nextDayProductId) >= 0;
                });
            }

            // 狀況一：反之，表示有新增的產品
            // 狀況二：產品數量相同，有可能新增一個產品再刪除另一個
            // 將第二天多的產品取出，計算完後，加入firstDay中
            if(firstDayProductIdLength <= nextDayProductIdLength) {
                productIds = nextDayProductIds.filter((nextDayProductId) => {
                    if (firstDayProductIds.indexOf(nextDayProductId) === -1) {
                        newProduct[nextDayProductId] = nextDaySpaces[spaceId].products[nextDayProductId];
                    }
                    return firstDayProductIds.indexOf(nextDayProductId) >= 0;
                })
            }
            // console.log('prepare into product map', i);
            productIds.map((productId, i) => {
                // console.log('in product map', i);
                let firstDayItemIds = Object.keys(firstDaySpaces[spaceId].products[productId].items);
                let firstDayItemIdLength = firstDayItemIds.length; // 第一天的產品項目數

                let nextDayItemIds = Object.keys(nextDaySpaces[spaceId].products[productId].items);
                let nextDayItemIdLength = nextDayItemIds.length; // 第二天的產品項目數
                
                let newItem = {};
                let itemIds;

                // 第一天的項目數比第二天的多，表示有項目被刪除
                // 處理第二天的就好
                if (firstDayItemIdLength > nextDayItemIdLength) {
                    itemIds = nextDayItemIds.filter((nextDayItemId) => {
                        return firstDayItemIds.indexOf(nextDayItemId) >= 0;
                    });
                }

                // 狀況一：反之，表示有新增的項目
                // 狀況二：項目數量相同，有可能新增一個項目再刪除另一個
                // 將第二天多的項目取出，計算完後，加入firstDay中
                if(firstDayItemIdLength <= nextDayItemIdLength) {
                    itemIds = nextDayItemIds.filter((nextDayItemId) => {
                        if(firstDayItemIds.indexOf(nextDayItemId) === -1) {
                            newItem[nextDayItemId] = nextDaySpaces[spaceId].products[productId].items[nextDayItemId];
                        }
                        return firstDayItemIds.indexOf(nextDayItemId) >= 0;
                    })
                }

                // console.log('prepare into item map', i);
                itemIds.map((itemId, i) => {
                    let firstUsageIds = Object.keys(firstDaySpaces[spaceId].products[productId].items[itemId].usage);
                    let firstUsageLength = firstUsageIds.length; // 第一天的用量計算規則數

                    let nextUsageIds = Object.keys(nextDaySpaces[spaceId].products[productId].items[itemId].usage);
                    let nextUsageLength = nextUsageIds.length; // 第二天的用量計算規則數

                    let newUsage = {};
                    let usageIds;

                    // 第一天的用量計算規則數比第二天的多，表示有用量計算規則被刪除
                    // 處理第二天的就好
                    if (firstUsageLength > nextUsageLength) {
                        usageIds = nextUsageIds.filter((nextUsageId) => {
                            return firstUsageIds.indexOf(nextUsageId) >= 0;
                        });
                    }

                    // 狀況一：反之，表示有新增的用量計算規則
                    // 狀況二：用量計算規則數量相同，有可能新增一個用量計算規則再刪除另一個
                    // 將第二天多的用量計算規則取出，計算完後，加入firstDay中
                    if(firstUsageLength <= nextUsageLength) {
                        usageIds = nextUsageIds.filter((nextUsageId) => {
                            if(firstUsageIds.indexOf(nextUsageId) === -1) {
                                newUsage[nextUsageId] = nextDaySpaces[spaceId].products[productId].items[itemId].usage[nextUsageId];
                            }
                            return firstUsageIds.indexOf(nextUsageId) >= 0;
                        })
                    }

                    // console.log('prepare into usage map', i);
                    usageIds.map((usageId) => {
                        let firstDayProductItemCost = firstDaySpaces[spaceId].products[productId].items[itemId].usage[usageId].cost || '';
                        let nextDayProductItemCost = nextDaySpaces[spaceId].products[productId].items[itemId].usage[usageId].cost || '';

                        let firstDayProductItemQuantity = firstDaySpaces[spaceId].products[productId].items[itemId].usage[usageId].quantity;
                        let nextDayProductItemQuantity = nextDaySpaces[spaceId].products[productId].items[itemId].usage[usageId].quantity
                        if (!firstDayProductItemCost || !nextDayProductItemCost) {
                            return;
                        }

                        let cost = nextDayProductItemCost - firstDayProductItemCost;
                        let quantity = nextDayProductItemQuantity - firstDayProductItemQuantity;

                        // 每個月的 2 號，cost 會歸零，cost會為負值
                        // 故這一天直接用的 nextDay 的 cost
                        if(cost < 0) {
                            cost = nextDayProductItemCost;
                        }

                        if (quantity < 0) {
                            quantity = nextDayProductItemQuantity;
                        }
                        
                        firstDaySpaces[spaceId].products[productId].items[itemId].usage[usageId].cost = cost;
                        firstDaySpaces[spaceId].products[productId].items[itemId].usage[usageId].quantity = quantity;

                    });
                    // console.log('leave usage map', i);
                    // 將新增的用量計算規則加回
                    let newUsageId = Object.keys(newUsage);
                    if(newUsageId.length > 0) {
                        // console.log('add new usages', i);
                        firstDaySpaces[spaceId].products[productId].items[itemId].usage = Object.assign(firstDaySpaces[spaceId].products[productId].items[itemId].usage, newUsage);
                    }
                });
                // console.log('leave items map', i);
                // 將新增的項目加回
                let newItemId = Object.keys(newItem);
                if(newItemId.length > 0) {
                    // console.log('add new items', i);
                    firstDaySpaces[spaceId].products[productId].items = Object.assign(firstDaySpaces[spaceId].products[productId].items, newItem);
                }
            });
            // console.log('leave products map', i);
            // 將新增的產品加回
            let newProductId = Object.keys(newProduct);
            if(newProductId.length > 0) {
                // console.log('add new product', i);
                firstDaySpaces[spaceId].products = Object.assign(firstDaySpaces[spaceId].products, newProduct);
            }
        });
        // console.log('leave spaces map', i);
        // 將新增的space加回
        let newSpaceId = Object.keys(newSpace);
        if(newSpaceId.length > 0) {
            // console.log('add new space', i);
            firstDaySpaces = Object.assign(firstDaySpaces, newSpace);
        }
        return firstDaySpaces;
    }

    module.exports = new usageController();
}());