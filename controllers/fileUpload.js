(function () {
    const cloudantDBMdl = require('../models/cloudantdb');
    const ibmWAAPI = require('../services/ibmWatsonAssistantAPI');
    const ibmCFAPI = require('../services/ibmCloudFoundaryAPI');
    function fileUploadController() {};

    fileUploadController.prototype.uploadFileToTrain = function(req, res) {
        let access_token = req.body.access_token;
        let token_type = req.body.token_type;
        let workspaceId = req.params.workspaceid;
        let cloudantdbGuid = req.params.dbserviceguid
        let watsonGuid = req.params.waserviceguid;
        let intentsAndDialogs = req.body.uploadData.intentsAndDialogs;
        let entities = req.body.uploadData.entities;

        let logTableName = 'file_upload_log';

        let failMessages = [];

        let pushEntities = {};
        let pushIntent = {};
        let pushDialog = {};

        return ibmCFAPI.getServicekeys(access_token, token_type, watsonGuid).then((result) => {
            let WAusername = result.resources[0].entity.credentials.username;
            let WApassword = result.resources[0].entity.credentials.password;
            return ibmWAAPI.initWatson(WAusername, WApassword);
        }).then((result) => {
            if (!result) {
                return Promise.reject(new Error('watson init fail'));
            }
            req.watson = result;
            return ibmCFAPI.getServicekeys(access_token, token_type, cloudantdbGuid);
        }).then((result) => {
            let DBusername = result.resources[0].entity.credentials.username;
            let DBpassword = result.resources[0].entity.credentials.password;
            return cloudantDBMdl.initDB(DBusername, DBpassword);
        }).then((result) => {
            if (!result) {
                return Promise.reject(new Error('DB init fail'));
            }
            req.DB = result;
            return cloudantDBMdl.get(req.DB, logTableName)
        }).then((result) => {
            if(!result) {
                return cloudantDBMdl.createDB(req.DB, logTableName).then((result) => {
                    if(!result) {
                        return Promise.reject(new Error('DB create fail'));
                    }
                    let indexContent = {
                        name: 'timestamp',
                        type: 'json',
                        index: {
                            fields:[
                                {'timestamp': 'desc'}
                            ]
                        }
                    }
                    return cloudantDBMdl.DBindex(req.DB, logTableName, indexContent).then((result) => {
                        if(!result) {
                            return Promise.reject(new Error('DB index create fail'));
                        }
                        return;
                    });
                })
            }
            return;
        }).then(() => {
            let type;
            entities.map((entity) => {
                let dataKeys = Object.keys(entity);
                let value;
                let synonyms;
                dataKeys.map((dataKey) => {
                    if (dataKey === '分類'){
                        if(type != entity[dataKey]) {
                            pushEntities[entity[dataKey]] = {
                                description: '',
                                values: []
                            };
                        }
                        type = entity[dataKey];
                    }
                    if(dataKey === '專有名詞') {
                        value = entity[dataKey];
                    }
                    if(dataKey === '別稱') {
                        synonyms = entity[dataKey].split('、');
                    }
                })
                pushEntities[type].entity = type;
                pushEntities[type].description = type;
                pushEntities[type].values.push({
                    value: value,
                    synonyms: synonyms
                });
            })
        }).then(() => {
            let pushEntityKeys = Object.keys(pushEntities);
            return Promise.all(pushEntityKeys.map((pushEntityKey) => {
                return ibmWAAPI.getEntity(req.watson, workspaceId, pushEntityKey).then((result) => {
                    if(result) {
                        let getEntity = result.entity;
                        let put = {
                            entity: getEntity,
                            new_entity: getEntity,
                            new_description: pushEntities[getEntity].description,
                            new_values: pushEntities[getEntity].values
                        }
                        return ibmWAAPI.putEntity(req.watson, workspaceId, put);
                    }
                    return ibmWAAPI.insertEntity(req.watson, workspaceId, pushEntities[pushEntityKey]);
                }).then((uploadResult) => {
                    if(uploadResult.message) {
                        return cloudantDBMdl.insert(req.DB, logTableName, uploadResult).then((result) => {
                            if(!result) {
                                return Promise.reject(new Error('fail to insert/update entity'));
                            }
                            return Promise.resolve({
                                type: 'entity',
                                value: pushEntityKey,
                                result: false,
                                message: uploadResult.message
                            });
                        });
                    }
                    return Promise.resolve({
                        type: 'entity',
                        value: pushEntityKey,
                        result: true,
                        message: null
                    });
                })
            }));
        }).then((results) => {
            failMessages = failMessages.concat(results);
            return Promise.all(intentsAndDialogs.map((intentsAndDialog) => {
                
                let intent = {
                    intent: '',
                    description: '',
                    examples: []
                };
                let dialog = {
                    dialog_node: '',
                    description: '',
                    conditions: '',
                    title: '',
                    output: {
                        generic: []
                    }
                };
                let intentsAndDialogKeys = Object.keys(intentsAndDialog);

                intentsAndDialogKeys.map((intentsAndDialogKey) => {
                    if(intentsAndDialogKey === '分類') {
                        intent.description = intentsAndDialog[intentsAndDialogKey];

                        dialog.dialog_node = intentsAndDialog[intentsAndDialogKey];
                        dialog.description = intentsAndDialog[intentsAndDialogKey];
                        dialog.title = intentsAndDialog[intentsAndDialogKey];
                    }
                    if(intentsAndDialogKey === '標準問題') {
                        intent.intent = intentsAndDialog[intentsAndDialogKey];
                        pushIntent[intent.intent] = {};

                        dialog.dialog_node = dialog.dialog_node + '-' + intentsAndDialog[intentsAndDialogKey];
                        dialog.conditions = '#' + intentsAndDialog[intentsAndDialogKey];
                        pushDialog[dialog.dialog_node] = {};
                    }
                    if(intentsAndDialogKey.indexOf('延伸問題') >= 0) {
                        intent.examples.push({
                            text: intentsAndDialog[intentsAndDialogKey]
                        });
                    }
                    if(intentsAndDialogKey === '標準答案') {
                        dialog.output.generic.push({
                            values: [{
                                text: intentsAndDialog[intentsAndDialogKey]
                            }],
                            response_type: 'text'
                        });
                    }
                });
                pushIntent[intent.intent] = Object.assign(pushIntent[intent.intent], intent);
                pushDialog[dialog.dialog_node] = Object.assign(pushDialog[dialog.dialog_node], dialog);
            }));
        }).then(() => {
            let pushIntentKeys = Object.keys(pushIntent);
            return Promise.all(pushIntentKeys.map((pushIntentKey) => {
                return ibmWAAPI.getIntent(req.watson, workspaceId, pushIntentKey).then((result) => {
                    if(result) {
                        let getIntent = result.intent;
                        let put = {
                            intent: getIntent,
                            new_title: pushIntent[getIntent].title,
                            new_intent: pushIntent[getIntent].intent,
                            new_description: pushIntent[getIntent].description,
                            new_examples: pushIntent[getIntent].examples
                        }
                        return ibmWAAPI.putIntent(req.watson, workspaceId, put);
                    }
                    return ibmWAAPI.insertIntent(req.watson, workspaceId, pushIntent[pushIntentKey]);
                }).then((uploadResult) => {
                    if(uploadResult.message) {
                        return cloudantDBMdl.insert(req.DB, logTableName, uploadResult).then((result) => {
                            if(!result) {
                                return Promise.reject(new Error('fail to insert/update intent'));
                            }
                            return Promise.resolve({
                                type: 'intent',
                                value: pushIntentKey,
                                result: false,
                                message: uploadResult.message
                            });
                        });
                    }
                    return Promise.resolve({
                        type: 'intent',
                        value: pushIntentKey,
                        result: true,
                        message: null
                    });
                })
            }));
        }).then((results) => {
            failMessages = failMessages.concat(results);
            let pushDialogKeys = Object.keys(pushDialog);
            return Promise.all(pushDialogKeys.map((pushDialogKey) => {
                return ibmWAAPI.getDialog(req.watson, workspaceId, pushDialogKey).then((result) => {
                    if(result) {
                        let getDialog = result.dialog_node;
                        let previous_sibling = result.previous_sibling;
                        let put = {
                            dialog_node: getDialog,
                            new_previous_sibling: previous_sibling,
                            new_dialog_node: pushDialog[getDialog].dialog_node,
                            new_description: pushDialog[getDialog].description,
                            new_conditions: pushDialog[getDialog].conditions,
                            new_output: pushDialog[getDialog].output
                        };
                        return ibmWAAPI.putDialog(req.watson, workspaceId, put);
                    }
                    return ibmWAAPI.insertDialog(req.watson, workspaceId, pushDialog[pushDialogKey]);
                }).then((uploadResult) => {
                    if(uploadResult.message) {
                        return cloudantDBMdl.insert(req.DB, logTableName, uploadResult).then((result) => {
                            if(!result) {
                                return Promise.reject(new Error('fail to insert/update dialog'));
                            }
                            return Promise.resolve({
                                type: 'dialog',
                                value: pushDialogKey,
                                result: false,
                                message: uploadResult.message
                            });
                        });
                    }
                    return Promise.resolve({
                        type: 'dialog',
                        value: pushDialogKey,
                        result: true,
                        message: null
                    });
                })
            }));
        }).then((results) => {
            failMessages = failMessages.concat(results);
            return failMessages;
        }).then((data) => {
            let resJson = {
                status: 1,
                data: data,
                msg: 'success to upload file to watson assistant'
            };
            res.status(200).json(resJson);
        }).catch((error) => {
            let resJson = {
                status: 0,
                msg: error
            };
            res.status(500).json(resJson);
        });
    }

    module.exports = new fileUploadController();
}());